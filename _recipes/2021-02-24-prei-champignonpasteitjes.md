---
layout: recipe
title: Prei-champignonpasteitjes
show: true
source: EVA
source_url: https://www.evavzw.be/recept/prei-champignonpasteitjes
duration: "30 min"
tags: [veggi]
size: 2 personen
---


### Ingrediënten voor 2 personen

* 1 el zonnebloemolie
* 1 middelgrote prei  (in dunne ringen)
* 250 g kastanjechampignons (schoongeborsteld en grof gesneden)
* 2 tenen knoflook (geperst)
* 5 el sojakookroom
* flinke snuf  zeezout
* zwarte peper
* 1 rol kant-en-klaar, zuivelvrij bladerdeeg

### Bereiding

Verwarm de oven voor tot 200 °C.  
  Verhit de olie in een koekenpan op middelhoog vuur en bak hierin de prei 4-5 minuten, tot hij zacht is. Voeg de champignons toe en bak alles nog 5 minuten, onder af en toe roeren.  
  Voeg de knoflook toe en bak hem 2 minuten, tot hij zacht is en begint te geuren. Schenk de kookroom erbij en laat alles nog 1 minuut sudderen. Breng het geheel tot slot op smaak met een flinke snuf zeezout en zwarte peper en haal de pan van het vuur.  
  Rol het bladerdeeg uit. Plaats twee kleine (pastei)vormen ondersteboven op het deeg en snijd er met een mes omheen. Als je wilt, kun je het overtollige bladerdeeg gebruiken om vormpjes uit te snijden om de bovenkant van de pasteitjes mee te versieren.  
  Vul de (pastei)vormen met het romige prei-champignonmengsel en leg de uitgesneden bladerdeegrondjes erbovenop; duw de buitenrand stevig aan, zodat de vulling er in de oven niet uit kan lekken, en druk met een vork ribbels in de rand. Bak de pasteitjes 15-18 minuten in de oven, tot de bovenkant goudgeel kleurt.  
  TIP
Voor een mooi krokant, goudgeel resultaat bestrijk je het bladerdeeg voor het bakken met een beetje sojadrink.  
  
