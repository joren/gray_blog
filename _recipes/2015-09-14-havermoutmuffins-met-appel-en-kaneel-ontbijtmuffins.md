---
layout: recipe
title: Havermoutmuffins met appel en kaneel | ontbijtmuffins
favorite: false
show: true
source: Voedzaam en snel
source_url: http://www.voedzaamensnel.nl/ontbijt/havermoutmuffins-met-appel-en-kaneel-ontbijtmuffins/
duration: "1 min"
tags: [havermout, muffins, ontbijt]
---

### Ingrediënten

#### Muffins
* 125g havermout
* 1 banaan
* 3 appels
* 2 eiren
* 150ml melk
* 2 theelepers Kaneel
* 75gr rozijnen
* 40g amandelen
* 40g walnoten

### Bereiding 

Verwarm de oven voor op 180 graden 
Meng de havermout, banaan, eieren, melk, kaneel en rozijnen samen met één appel (geschild en van klokhuis ontdaan) in een keukenmachine 
Hak de noten grof, schil de andere twee appels en snijd ze in kleine partjes 
Roer de noten en appelpartjes door het beslag en schep dit in 6 - 8 muffinvormpjes (het liefst een flexibele vorm, dan krijg je ze er makkelijk uit. Gebruik je stevige papieren vormpjes, dan kun je ze beter uit de vormpjes eten) 
Plaats de havermoutmuffins voor 30 minuten in de oven 
Laat de muffin even afstomen, haal ze uit de felxibele vorm door deze voorzichtig om te keren en serveer de ontbijtmuffins lauwwarm