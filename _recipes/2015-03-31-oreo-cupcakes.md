---
layout: recipe
title: Oreo cupcakes
favorite: false
show: true
source: Veerle
duration: "120 min"
tags: [oreo, dessert]
---

### Ingrediënten

#### Cupcakes
* 3 eieren
* 115 g zachte boter
* 225 g fijne tafelsuiker
* 125 ml melk
* 180 g zelfrijzend bakmeel
* 1 el cacaopoeder
* 12 + 8 oreo

#### Cream cheese frosting
* 200 g Philadelphia
* 100 g zachte boter
* 125 g poedersuiker
* 1 tl vanille extract
* 3 à 4 oreo
* 12 mini oreo om te decoreren

### Bereiding 

## Bereiding

Maak eerst de frosting, deze moet 1,5 uur opstijven in de koelkast. Meng eerst de roomkaas, de boter en het vanille extract met een mixer tot deze een gladde massa vormt. Voeg daarna de poedersuiker beetje bij beetje toe (mixer op de laagste stand!), voeg er vervolgens de verkruimelde oreo’s aan toe.

Verwarm de oven voor op 160°. Klop de eieren samen op met de boter en de suiker. Voeg er vervolgens het bakmeel, de melk en het cacaopoeder aan toe. Mix dit geheel een 2-tal minuten met de mixer zodat het een gladde massa vormt. Voeg vervolgens de 8 verkruimelde oreo’s toe.

Leg in elk papieren vormpje een oreo en schep er vervolgens het beslag in. Plaats de cupcakes 16-18 minuten in de oven.

Laat de cupcakes even afkoelen in de vorm en plaats ze erna op een rooster. Als ze volledig zijn afgekoeld kan je ze decoreren met de frosting en de mini oreo’s.