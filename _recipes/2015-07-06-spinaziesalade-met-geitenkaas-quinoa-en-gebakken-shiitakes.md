---
layout: recipe
title: Spinaziesalade met geitenkaas, quinoa en gebakken shiitakes
favorite: false
show: true
source: Dagelijke kost
source_url: http://www.een.be/programmas/dagelijkse-kost/recepten/spinaziesalade-met-geitenkaas-quinoa-en-gebakken-shiitakes?&personen=2
duration: "15 min"
tags: []
---

### Ingrediënten

### Bereiding 

de quinoa: 
Ontdooi een portie groentebouillon, of doe een kleine moeite en maak een pot verse groentebouillon klaar. 
Giet de quinoa in een zeef en spoel de zaadjes onder stromend water. Laat de zaadjes even uitlekken. 
Zet een steelpannetje of een kookpot op een zacht vuur. Kook de quinoa gaar in de bouillon. Doe dit in een verhouding van 1/3 quinoa en 2/3 groentebouillon. 
De zaadjes zullen de bouillon absorberen. Proef de quinoa en voeg indien nodig een beetje extra bouillon toe. 
de vinaigrette: 
Neem een mengschaal en doe er de honing en de mosterd in. 
Schenk er de rode wijnazijn bij en meng alles met de garde. 
Blijf ritmisch roeren met de garde en schenk er beetje bij beetje de walnotenolie en de olijfolie bij, tot de vinaigrette bindt. 
Proef en kruid het sausje met wat peper van de molen en een snuifje zout. 
Zet de schaal met vinaigrette even opzij. 
TIP: Je kunt achteraf ook nog wat verbrokkelde walnoten toevoegen. 
de shiitakes: 
De oosterse paddenstoelen hebben een steeltje dat wat taaier is. Om de shiitakes te garen snijd of scheur je elke paddenstoel in 4 partjes. 
Smelt een klontje boter in een braadpan op een matig vuur. 
Bak de stukjes paddenstoel. De boter wordt snel opgeslorpt, dus voeg indien nodig een extra beetje boter toe of een scheutje olijfolie. 
Pel intussen een teentje look en snij het in piepkleine stukjes. Plet de look tot pulp en voeg die toe aan de bakkende shiitakes. 
Schud de paddenstoelen regelmatig op. Kruid de shiitakes tenslotte met wat peper en zout. Na enkele minuten zijn de paddenstoelen al gaar. 
de salade: 
Vul de gootsteen met koud water en spoel er de spinazieblaadjes in. Doe het voorzichtig, zodat je de fijne spinazie niet kneust. Als je ijsblokjes in het water doet, blijft de groente extra krokant. Laat de spinazie nadien uitlekken in een vergiet (of op een handdoek). 
Let wel: Wanneer je noodgedwongen geen jonge spinazie gebruikt, scheur dan de taaiste bladnerf van de blaadjes en scheur ze in kleinere stukken. Laat de spinazie uitlekken en strooi ze in een ruime slakom. 
Spoel de lente-uitjes en snij de wortelstukjes eraf. Snipper het bleke en lichtgroene deel van de uitjes in flinterdunne ringen. 
Neem er een ruime slakom bij en doe daar de blaadjes spinazie in. 
Spoel de blaadjes zuring en de Romeinse kervel. Verwijder de steeltjes. Scheur de blaadjes in stukjes en voeg ze toe aan de salade. Meng het groen voorzichtig. 
Verbrokkel de geitenkaas en strooi de hagelwitte stukjes verse kaas over het groen. 
Nu over naar de warme elementen uit de salade. Schep de gebakken shiitakes in de slakom, gevolgd door de gare quinoa. 
Schenk er een portie van de vinaigrette over. Let erop dat de sla niet zwemt in het sausje. Zet eventueel een restje vinaigrette apart op de tafel. 
Meng de frisse ingrediënten van de sla met de warme. Doe dit voorzichtig en met zorg. Serveer meteen.