---
layout: recipe
title: Quiche met roquefort, appel en witloof
show: true
source: Dagelijkse Kost
source_url: https://dagelijksekost.een.be/gerechten/quiche-met-roquefort-appel-en-witloof
duration: "30 + 50 min"
tags: [veggi, quiche]
size: 4 personen
---

### Ingrediënten

#### De quiche
* boter
* bloem
* 1 vel kruimeldeeg (of bladerdeeg)
* 1 ajuin
* 2 appels (Jonagold)
* 5 stronkjes witloof
* pepers
* zout
* nootmuskaat
* 1 snuifje kristalsuiker
* 4 eieren
* 2 dl room
* 200 g blauwe kaas

#### De vinaigrette
* 1 eetlepel mosterd
* 1 koffielepel honing
* 1 scheutje dragonazijn
* slaolie
* pepers
* zout

#### De Salade
* 1 bussel waterkers
* 1 vierde bussel peterselie
* 1 vierde bussel dragon
* een handvol walnoten
* 1 appel (Granny Smith)

### Bereiding

#### De quiche
1. Verwarm de oven voor tot 180 °C.
2. Vet een taartvorm in met boter. Bestuif met bloem.
3. Leg het kruimeldeeg in de vorm. Duw de randen goed aan. Snijd overtollig deeg weg.
4. Leg een vel boterpapier op het deeg. Strooi er de blindbakvulling in.
5. Bak de quichebodem 20 minuten in de voorverwarmde oven.
6. Smelt een klont boter in een pan.
7. Pel en snipper de ui. Schil de appelen en snijd ze in stukken. 
8. Stoof de ui kort aan. Doe er de appelen bij.
9. Verwijder de harde kern van het witloof en snijd het in grote stukken.
10. Doe het witloof bij de appelen en laat mee bakken. Kruid met zout, peper, nootmuskaat en een snuifje suiker.
11. Klop de eieren los met de room. Kruid met peper, zout en nootmuskaat.
12. Haal de taartbodem uit de oven. Leg er de gebakken groenten in. Giet er het eimengsel over.
13. Verbrokkel de kaas en strooi hem over de quiche.

TIP  Als je de smaak van blauwe kaas te sterk vindt, kun je ook gewone gemalen kaas gebruiken of de quiche zonder kaas bereiden.

14. Bak de quiche 30 minuten in de voorverwarmde oven.

#### De vinaigrette
15. Meng de mosterd met honing en azijn. Klop er de slaolie door met een garde.
16. Breng op smaak met peper en zout.

#### De salade
17. Spoel de waterkers, peterselie en dragon. Pluk de blaadjes.
18. Hak de walnoten fijn en doe ze bij de salade.
19. Snijd een Granny Smith appel in fijne staafjes (julienne) en meng ze door de salade.
