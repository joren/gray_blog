---
layout: recipe
title: Broccoli / kerstomaat / scampi
favorite: false
show: true
source: Au Bain-Marieke
source_url: http://aubainmarieke.be/recipe-items/broccoli-kerstomaat-scampi/
duration: "30 min"
tags: [scampi, oven]
---

### Ingrediënten

* 150ml room
* 1 klein doosje boursin (geen brousin cuisine)
* 1 groot ei
* 2 stuks broccoli
* genoeg volkoren pasta
* 1 groot handvol kerstomaten
* 500g ontdooide/ontdarmde/gepelde (en "droge") scampi
* 1 el paneermeel
* veel gemalen kaas
* snuif peper en zout
* beetje olijfolie
* 1 teen look

### Bereiding 

Instructions

1. Kook je roosjes broccoli beetgaar in ruim water, zonder deksel. 
2. Halveer je kerstomaten. 
3. Bak je scampi kort aan, aan beide kanten, kruid met peper en zout. 
4. Kook je pasta al dente. 
5. Wrijf een ruime ovenschaal in met je geperst teentje look en wat olijfolie. 
6. Klop je ei los, meng dit met de room, meng er je boursin door en kruid met peper en eventueel nog wat zout (mag niet te flets zijn, want het moet je ovenschotel op smaak brengen). 
7. Meng je groente, scampi en pasta door elkaar, overgiet met je roommengsel en meng eventueel nog eens. 
8. Voor een krokant laagje gebruik je een combinatie van paneermeel en gemalen kaas. 
9. Schuif in de oven onder de gril tot je een mooi korstje hebt. Smakelijk!