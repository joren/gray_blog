---
layout: recipe
title: Bloemkoolcouscous met granaatappel, munt en tahindressing
favorite: false
show: true
source: EVA
source_url: "https://www.evavzw.be/recept/bloemkoolcouscous-met-granaatappel-munt-en-tahindressing"
duration: "30 min"
tags: [groenten, veggi]
---

### Ingrediënten
* 3 gele paprika's
* 1 bloemkool
* handvol notenmengeling naar keuze
* 4 el tahin
* 4 el water
* 1 citroen
* snuif komijnpoeder
* verse munt
* verse koriander
* verse peterselie
* snuif peper en zout

### Bereiding 

1. Halveer de paprika’s en verwijder de zaadlijsten. Plaats ze op een met bakfolie voorziene bakplaat en rooster gedurende 30 minuten in de oven op 200°C. 
2. Snijd de bloemkool in roosjes. Plaats de roosjes in een foodprocessor of blender en pulse tot de bloemkool de textuur van droge rijst krijgt. Roerbak de ‘rijst’/couscous zeer kort aan in een pan, tot ze ‘al dente’ is. Voeg het sap van een halve citroen toe, alsook een beetje zeste. 
3. Ontpit de granaatappel, snijd de muntblaadjes, koriander en peterselie fijn, hak de noten grof. Meng onder de bloemkoolcouscous. 
Voor de tahindressing, mix je de tahini en komijnpoeder met een klein beetje water (goed mengen met een vork, of shaken in een fles tot het een beetje schuimig wordt). 
4. Vul de paprika’s met het bloemkoolcouscous mengsel en dresseer met de tahindressing.
