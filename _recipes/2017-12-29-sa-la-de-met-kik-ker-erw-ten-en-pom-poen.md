---
layout: recipe
title: Salade met kikker­erwten en pompoen
favorite: true
show: true
source: Albert Hein
source_url: https://www.ah.be/allerhande/recept/R-R1188528/salade-met-kikkererwten-en-pompoen-van-ella-mills
duration: "30 min"
tags: [veggi, oven, vegan]
size: 2 personen
---

### Ingredienten

* 80 g zongedroogde tomaten
* 60g rucola
* 2 tl honing
* 1 tl kurkuma
* 1 el appelcider- of wittewijnazijn
* 1 tl chilipoeder
* 160 g of 1 blik kikkererwten
* 4 el olijolie
* 2 tl Provençaalse kruiden
* 2 tl paprikapoeder
* 250 g of 1 kleine Butternut pompoen

### Bereiding 

1. Verwarm de oven voor op 220 °C. Leg de pompoenstukjes op een met bakpapier beklede bakplaat en bestrooi met de paprikapoeder, Provençaalse kruiden en eventueel zout. Besprenkel met de helft van de olijfolie. Bak de pompoen ca. 30 min. in het midden van de oven. 
2. Verdeel ondertussen de kikkererwten over een andere met bakpapier beklede bakplaat en schep om met de chilipoeder. Plaats de bakplaat met de kikkererwten onder de pompoen in de oven en bak de laatste 20 min. mee tot ze stevig zijn, maar niet knapperig. Neem de pompoen en de kikkererwten uit de oven en laat in ca. 1 uur afkoelen. 
3. Meng ondertussen de rest van de olijfolie met de azijn, geelwortel en honing tot een dressing en breng op smaak met veel peper en eventueel zout. Meng de pompoen en kikkererwten met de rucola en de zongedroogde tomaten. Schenk de dressing erover en meng goed door elkaar.

Ook gewoon lekker als het nog warm is.
