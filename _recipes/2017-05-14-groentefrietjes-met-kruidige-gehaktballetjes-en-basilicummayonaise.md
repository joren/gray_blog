---
layout: recipe
title: Groentefrietjes met kruidige gehaktballetjes en basilicummayonaise
favorite: false
show: true
source: Lekker van bij ons
source_url: https://www.lekkervanbijons.be/recepten/groentefrietjes-met-kruidige-gehaktballetjes-en-basilicummayonaise
duration: "40 min"
tags: [voorgerecht, aperitief, groenten]
---

### Ingrediënten

#### GROENTEFRIETJES
* 1/4 knolselder
* 2 zoete aardappelen
* 1 pastinaak
* 1 courgette
* 2 teentjes look
* 1 takje tijm
* 1 el gedroogde oregano

#### BASILICUMMAYONAISE
* 1/2 bosje basilicum
* 3 el mayonaise

#### GEHAKTBALLETJES
* 500 g gehakt
* 1 gesnipperde sjalot
* 1 fijngesneden chili zonder zaadjes
* 2 el gehakte peterselie
* 2 el gehakte dille
* 3 el olijfolie
* snuifje peper en zout

### Bereiding 

Verwarm de oven voor op 200°C. Spoel de knolselder, zoete aardappelen, courgette en pastinaak. Snijd de groenten ongeschild in balkjes.

Bekleed een bakplaat met bakpapier en schik hierop de groentefrietjes. Pers vervolgens de teentjes knoflook uit boven de groenten. Rits de tijm van de takjes en verdeel samen met de gedroogde oregano over de frietjes. Besprenkel met 2 eetlepels olijfolie en kruid met peper en zout. Zet 20 min. in de voorverwarmde oven. Roer regelmatig om.

Snijd de basilicum heel fijn en mix door de mayonaise. Kruid met peper en zout.

Meng de sjalot, gehakte peterselie, dille, knoflook, gesnipperde chili door het gehakt. Rol balletjes van het gehakt. Verhit 1 eetlepel olijfolie in een pan en bak de balletjes goudbruin in 6-8 min.

Haal de groentefrietjes uit de oven en serveer met de gehaktballetjes.