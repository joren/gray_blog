---
layout: recipe
title: 'Gyros van geroosterde groente met kokosyoghurt-tzatziki'
favorite: true
show: true
source: EVA
source_url: https://www.evavzw.be/recept/gyros-van-geroosterde-groente-met-kokosyoghurt-tzatziki
duration: "45 min"
tags: [veggi, pita]
size: 4 personen
---

### Ingrediënten

* 2 paprika's (geen groene)
* 3 courgette
* 1 rode ajuin
* 1 bloemkool
* een blik kikkererwten van 400 g
* cayennepeper
* garam massala
* 120 g zuivelvrije kokosyoghurt
* verse munt
* 1 citroen
* 1 komkommer
* knoflook
* 4 pitabroodjes
* verse granaatappelpitjes
* olijfolie
* zout en peper

### Bereiding

Verwarm de oven voor tot 180°C.

Haal de zaadjes uit de paprika en snijd ze in kleine stukjes. Snijd de courgette en ui ook. Breek de bloemkool in kleine roosjes. leg de groente op een bakplaat. Doe de kikkererwten erbij met wat van het vocht. Besprenkel met een flinke scheut olijfolie, doe er 2 theelepels cayennepeper, 2 theelepels garam massala en zout en peper bij. Meng alles door elkaar. Rooster 30 minuten in de oven.

Tijd voor de tzatziki. Doe de yoghurt in een kom. Doe hier een klein bosje fijngehakte munt, sap van 1 citroen en de geraspte komkommer bij. Rasp 1 teen knoflook erbij, breng op smaak en meng alles door elkaar.

Haal de groenten uit de oven en meng ze door elkaar. Zet ze dan nog 15 minuten in de oven.

Tijd om alles te combineren. Verwarm de pita's en snijd ze open. Vul ze met de groente, tzatziki, granaatappelpitjes en dan meer groente en meer tzatziki en granaatappelpitjes. Geniet ervan!
