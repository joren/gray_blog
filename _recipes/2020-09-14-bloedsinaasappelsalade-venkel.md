---
layout: recipe
title: 'Bloedsinaasappelsalade met pecannoten, cannellinibonen en gebakken venkel'
favorite: true
show: true
source: EVA
source_url: https://www.evavzw.be/recept/bloedsinaasappelsalade-met-pecannoten-cannellinibonen-en-gebakken-venkel
duration: "30 min"
tags: [veggi, salade]
size: 4 personen
---

### Ingrediënten

* olijfolie
* 2 grote venkelknollen in kwarten en daarna in plakken van 5 mm
* 4 bloedappelsienen van goede kwaliteit, of gewone appelsienen
* 100 g slamix (rucola en waterkers werken goed)
* 1 blik cannellinibonen à 400 g (uitgelekt en afgespoeld)
* 100 g pecannoten
* 75 g bleke rozijnen
* 10 g bieslook ieder sprietje in drieën
* zeezout en peper
* Voor de dressing:
* 2 el rodewijnazijn
* 1 el agavesiroop
* 1 tl kaneel
* 1/2 tl cayennepeper
* 1 el water
* een flinke scheut olijfolie


### Bereiding

Zet een grote koekenpan op een middelgroot vuur en sprenkel er wat olijfolie in. Voeg de venkel toe en bak een paar minuten, tot de venkel wat zachter is en aan de randjes goudbruin wordt. Schep met een schuimspaan in een kom, breng op smaak met wat zout en peper en zet apart.

Je hebt een scherp mes nodig om de sinaasappels te schillen. Snijd de boven- en onderkant van elke sinaasappel. Verwijder vervolgens reep voor reep van boven naar beneden de rest van de schil en het witte vlies, tot de hele sinaasappel geschild is. Snij de sinaasappel overdwars in plakjes van 1 cm dik.

Leg de slablaadjes op een grote schaal of in een ondiepe slakom. Verdeel de cannellinibonen erover, gevolgd door de sinaasappeschijfes, de pecannoten en de venkel. Bestrooi met de rozijnen en de bieslook.

Meng voor de dressing de rodewijnazijn, de agavesiroop, de kaneel, de cayennepeper, het water en de olijfolie in een kleine kom en sprenkel de dressing over de salade. Serveer direct.
