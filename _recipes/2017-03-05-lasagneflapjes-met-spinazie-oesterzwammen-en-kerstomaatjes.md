---
layout: recipe
title: Lasagneflapjes met spinazie, oesterzwammen en kerstomaatjes
favorite: false
show: true
source: EVA
source_url: http://www.evavzw.be/recept/lasagneflapjes-met-spinazie-oesterzwammen-en-kerstomaatjes
duration: "20 min"
tags: [pasta, vegan]
size: 4 personen
---

### Ingrediënten

* 250g lasagnevel
* 250g oesterzwammen
* 10 blaadjes verse salie
* 2 teentjes look
* 70g tomatenpuree
* 4dl sojaroom
* 1tl tijm
* 250g kerstomaatjes

### Bereiding 

Kook de lasagnevellen 3 minuten in gezouten water

Verwarm de oven voor op 180°C

Snijd de oesterzwammen in reepjes. Hak de salie fijn.

Bak de oesterzwammen op een hoog vuur goed bruin in olijfolie. Voeg aan het eind van de baktijd de pijnboompitjes toe en bak ze even mee. Kruid met salie, peper en zout.

Pel en snipper de knoflook.

Maak een tomatenroomsaus door de tomatenpuree koud te vermengen met 2 dl (soja)room, de gesnipperde knoflook, peper en zout.

Verwarm wat olijfolie in een grote pot, voeg de spinazie toe, laat hem slinken en roer er de rest van de sojaroom door. Kruid de spinazie met tijm, peper en zout.

Vet een ruime ovenschaal in.

Snijd de lasagnevellen doormidden.

Leg een half lasagnevel op de schaal, vul het met een laagje spinazie en daarop enkele oesterzwammen. Vouw het vel toe zodat je een driehoekig flapje krijgt. Ga zo door met de overige lasagnevellen. Smeer de koude tomatensaus over de flapjes, verdeel de kerstomaatjes over de ovenschaal. Kruid met peper en zout.

Zet de schaal 15 minuten in de oven.
