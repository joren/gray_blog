---
layout: recipe
title: Watermelon Pops
favorite: false
show: true
source: Orangette
source_url: http://orangette.blogspot.be
duration: "20 min"
tags: [ijs, dessert]
---

### Bereiding 

These popsicles will only taste as good as the watermelon you start with, so start with a sweet, flavorful one. Oh, and you can omit the vodka, if you want.

A roughly 3-pound (1.5-kg) chunk of watermelon 
½ cup (100 grams) sugar 
Big pinch of kosher salt 
1 tablespoon freshly squeezed lime juice, or to taste 
1 to 2 tablespoons vodka (optional)

Cut away and discard the rind of the watermelon, and cut the flesh into cubes. Chuck the cubes into a blender or food processor, and process until liquefied. Pour through a strainer (to remove seeds) into a large measuring cup. You should have about 3 cups (750 ml) of watermelon juice. (If you have more, well, drink up! Or freeze for future use.)

In a small, nonreactive saucepan, warm about ½ cup (125 ml) of the watermelon juice with the sugar and then salt, stirring until the sugar is dissolved. Remove from the heat, and stir this syrup into the remaining 2 ½ cups (625 ml) watermelon juice. Mix in the lime juice and vodka, if using. Taste, and add more lime juice, if you want, or more salt. You shouldn’t taste the salt; it’s just there to intensify the watermelon flavor.

Chill the mixture thoroughly - if the watermelon was refrigerator-cold when you started the process, this won't take long - and then pour it into your popsicle mold of choice. (I used this.) If you have more mixture than will fit in your popsicle molds, drink it, or for mini-pops(!) and other fun stuff, freeze it in ice-cube trays.

Yield: about 10 pops