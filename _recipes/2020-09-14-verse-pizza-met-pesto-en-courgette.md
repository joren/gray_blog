---
layout: recipe
title: Verse pizza met pesto en courgette
show: true
source: EVA
source_url: https://www.evavzw.be/recept/verse-pizza-met-pesto-en-courgette
duration: "45 min"
tags: [veggi, pizza]
size: 4 personen
---

### Ingrediënten

#### Voor de pesto
* 1 plantje basilicum
* 2 teentjes knoflook
* het sap van 1 citroen
* 4 eetlepels olijfolie
* 2 eetlepels edelgistvlokken
* 50 gram pijnboompitten
* peper en zout naar smaak

#### Voor de pizza
* 1 verpakking vers pizzadeeg
* 1 courgette , flinterdun gesneden (met een mandoline)
* 2 zoete ajuinen
* agavesiroop
* peper
* pijnboompitten als garnering

### Bereiding

We beginnen met de uien, want deze uien mag je even de tijd geven om goed te garen zodat ze die lekkere karamelsmaak krijgen. Snijd de uien beide in de lengte doormidden en snijd ze daarna in halve ringen. Zet het vuur medium/ laag en giet een scheutje olijfolie in de pan. Laat de uien goed bakken en voeg een eetlepel agavesiroop toe. Je kan de uien gerust een minuut of 20 geven. Wel goed blijven roeren, zodat ze niet verbranden! Een klein scheutje water bij de uien kan helpen om de smaken goed los te krijgen en om te voorkomen dat de uien verbranden.

Voeg alle ingrediënten voor de vegan pesto in de keukenmachine of blender. Mixen maar! Als het een mooi groen geheel is geworden, is het heel belangrijk om even de pesto te proeven. Hier kan je nog smaak toevoegen, mocht je dat nodig vinden. Voor iets zoeter voeg je wat meer agave toe, voor een zuurtje wat citroensap. Dat waren stap 1 en 2!

Verwarm je oven voor op 220 graden.

Pak nu het verse pizzadeeg erbij. Pak jouw gewenste bakvorm en bedek deze met bakpapier. Smeer het pizzadeeg goed (lekker dik!) in met de pesto. Dit is de saus van de pizza – weer eens wat anders dan de tomatensaus die we gewend zijn te gebruiken bij pizza’s! Bedek nu de pizza met de plakjes courgette. Voel je vrij om een leuk patroontje te leggen: je pizza mag er gerust mooi uitzien. Presentatie is key! Verdeel vervolgens de mooi gebruinde uienringen over de pizza.

Schuif nu je pizza voor 15-20 minuten in de oven. Let op: iedere oven is weer verschillend, dus houd je pizza gewoon een beetje in de gaten.

Als de pizza lekker warm uit de oven komt, maak je hem af door er (geroosterde) pijnboompitten en eventueel rucola overheen te strooien. Easy as! Toch?
