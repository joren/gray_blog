---
layout: recipe
title: Gebakken ravioli met spinazie-champignonsalade
favorite: true
show: true
source: Joren
duration: "15 min"
tags: [veggi]
---

### Ingrediënten
* eetlepel olijfolie
* 1 bosje lente-ui
* 4 el koolzaadolie
* 4 el mosterd
* 1 doosje kerstomaatjes
* 250 g spinazie
* 500 g champignons
* 500 g ravioli

### Bereiding 

Kook de ravioli 1 minuut in gezouten, kokend water. Schep ze voorzichtig uit de pan en laat ze uitlekken. Spoel ze af met koud water om te verhinderen dat ze aan elkaar kleven. 
Reinig de champignons en snijd ze in schijfjes. Was en droog de spinazie en hak ze grof. Snijd de kerstomaatjes doormidden en hak de lente-uitjes in kleine stukjes. 
Vermeng de zonnebloemolie en de mosterd. Meng alle groenten en roer er de mosterdolie door. Kruid met peper en zout. 
Verhit olie in twee grote bakpannen, verdeel de ravioli over de twee pannen en bak ze. Roer ze tijdens het bakken af en toe om. 
Verdeel de groentesalade over de borden en schep er de raviolikoekjes op.