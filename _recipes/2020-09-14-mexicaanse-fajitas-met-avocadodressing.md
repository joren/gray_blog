---
layout: recipe
title: Mexicaanse fajita's met avocadodressing
favorite: false
show: true
source: EVA
source_url: https://www.evavzw.be/recept/mexicaanse-fajitas-met-avocadodressing
duration: "20 min"
tags: [vegan, fajita]
size: 4 personen
---

### Ingrediënten

* 1 rode ajuin
* 2 zoete puntpaprika's
* 1 gele paprika
* 1 pakje veggie kipstukjes (in de meeste supermarkten)
* 1 el fajitakruiden (of: chilipoeder + paprikapoeder+ uienpoeder + knoflookpoeder + komijn + cayennepoeder)
* 1 avocado
* 1 limoen
* 4 (volkoren) tortillawrap
* peper en zout naar smaak

### Bereiding

Verwarm de oven voor op 220 graden Celsius en leg een bakplaat klaar met een vel bakpapier.

Begin met het wassen en snijden van de groenten en de vegetarische kipstuckjes. Snij de rode ui in dikke ringen en de paprika's in dikke repen (circa 1 cm breed).

Snij vervolgens de vegetarische kipstuckjes ook in reepjes. Doe alles in een grote kom en voeg de Mexicaanse kruiden toe. Hussel goed door elkaar.

Verdeel de inhoud van de kom over de bakplaat en bak 15 minuten in de voorverwarmde oven. Schep halverwege een keer inhoud om.

Blend de avocado met het sap van de limoen tot een gladde dressing. Voeg wat zout toe.

Bereid de tortilla's in de magnetron of koekenpan zoals de verpakking aangeeft.

Vul de tortilla's met de gegrilde groenten, vegetarische kipstuckjes en een flinke lepel avocado dressing.
