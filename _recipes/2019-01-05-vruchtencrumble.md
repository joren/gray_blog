---
layout: recipe
title: Vruchtencrumble
favorite: false
show: true
source: Schoonmoeder
duration: "30 min"
tags: [dessert]
---


### Ingrediënten

* schep vanilleijs
* 70g specula's
* 75g boter
* 75g bloem
* 6 eetlepels rietsuiker
* 1/2 citroensap
* 1 banaan
* 2 appels
* 2 peren

### Bereiding 

1. verkruimel de speculaas. (in een plastiek zakje doen en erop kloppen tot ge er bij neervalt) Voeg er de bloem 3 eetlepels rietsuiker en de in nootjes verdeelde boter bij.

2. Kneed met de vingertoppen tot een kruimelige massa of meng met de keukenrobot

3. Snij het geschilde fruit in stukjes en besprenkel met citroensap. Doe het fruit in een ovenschotel en strooi er 3 eetlepels rietsuiker over.

4. Verdeel er het speculaasmengsel over

5. Schuif gedurende 25 min in de voorverwarmde oven op 200°C