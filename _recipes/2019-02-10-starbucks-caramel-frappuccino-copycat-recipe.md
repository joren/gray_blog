---
layout: recipe
title: Starbucks® Caramel Frappuccino Copycat Recipe
favorite: false
show: true
source: All Recipes
source_url: https://www.allrecipes.com/recipe/235850/starbucks-caramel-frappuccino-copycat-recipe/
duration: "10 min"
tags: []
---

### Ingrediënten
* 3 tbs white sugar
* 1/3 cup caramel sauce
* 1 cup low fat milk
* 1 cup strong coffee, coxed
* 1 cups ice

### Bereiding 

Blend ice, coffee, milk, caramel sauce, and sugar together in a blender on high until smooth. Pour drink into two 16-ounce glasses.