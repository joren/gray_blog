---
layout: recipe
title: Groen­te­chi­li sin car­ne
favorite: false
show: true
source: Albert Hein
source_url: https://www.ah.be/allerhande/recept/R-R1185408/groentechili-sin-carne
duration: "35 min"
tags: [ah, mexicaans, veggi]
---

### Ingrediënten
* schepje zure room of Griekse yoghurt
* doos taco's
* 425 g flageolets (bonen, blik)
* 400 g zwarte bonen (blik)
* 300 g salsasaus mild
* 1 zak paprikamix (3 kleuren)
* 300 g zoete aardappel
* 1 tl gemalen komijn
* 1 el paprikapoeder
* 4 el arachideolie
* 1 courgette
* 1 aubergine

### Bereiding 

1. Snijd de aubergine en courgette in blokjes van 1 x 1 cm (brunoise). Verhit de olie in een hapjespan en bak de aubergine met de paprikapoeder, komijn en eventueel zout 5 min. op middelhoog vuur. Schep regelmatig om. 
2. Schil ondertussen de zoete aardappelen en snijd in blokjes van 1 x 1 cm. Maak de paprika’s schoon en snijd ook in blokjes van 1 x 1 cm. Voeg de aardappel toe aan de aubergine en bak 5 min. mee. Voeg de paprika en courgette toe en bak nog 3 min. Voeg de salsasaus toe en warm nog 5 min. mee. 
3. Laat ondertussen de zwarte bonen en flageolets uitlekken in een zeef en spoel af met koud water. Voeg de bonen toe aan de groenten en warm 3 min. mee. Breng de groentechili op smaak met peper en eventueel zout.

__Combinatietip:__ 
Lekker met tacoschelpen en Griekse yoghurt 0%