---
layout: recipe
title: Chocolade Granola
favorite: true
show: true
source: "Peaceful Cuisine"
source_url: "https://www.youtube.com/watch?v=tySKnyDVmFk"
duration: "10 min"
tags: [ontbijt, vegan]
---


### Ingrediënten

* 120g noten mengeling
* 300g havermout
* 60g kokos olie
* 30g gemalen kokospoeder
* 30g cacao poeder
* 100g chocolade korrels
* 120g esdoornsiroop of andere zoetstof zoals honing
* 1 tsp vanille extract
* snuifje zout

### Bereiding

1. Verwarm de oven voor op 160 graden
2. Als je geen kokosolie hebt maar een pasta, smelt deze dan op een laag vuurtje. Zet het vuur af zodra de pasta bijna helemaal opgelost is en laat even afkoelen.
3. Hak de noten fijn
4. Meng alle ingrediënten en voeg als laatste de kokosolie toe
5. Roer goed om zodat je 1 homogene massa krijgt
6. Kap de mengeling op en een bakplaat en verdeel zo gelijk mogelijk
7. Bak 30 minuten op 160 graden en roer eens om na 15 minuten
