---
layout: recipe
title: TIRAMISU MET LOTUS SPECULOOS
title: Tiramisu met Lotus Speculoos
favorite: true
show: true
source: Lotus
source_url: https://www.lotusbakeries.be/nl/ricette/tiramisu-met-lotus-speculoos
duration: "30 min"
tags: [dessert]
size: 4/8 personen
---

### Ingrediënten

Volgens Lotus voor 8 personen, al maken we dit vaak gewoon voor ons twee :)

* 3 eieren
* 6 el suiker
* 2 el amaretto
* 500 g mascarpone
* 250 g Lotus speculoos
* 1 kop sterke koffie
* 2 el cacaopoeder

### Bereiding

1. Splits de eieren en klop de eierdooiers en de suiker op tot een schuimig mengsel.
2. Parfumeer het mengsel met de amaretto en schep er de mascarpone onder.
3. Klop de eiwitten op en spatel voorzichtig onder het mascarponebeslag.
4. Verdeel de speculoos over glazen en giet er een scheut koffie op. Bedek met een laag mascarponecrème. Herhaal deze lagen tot alle ingrediënten op zijn.
5. Werk af met gezeefd cacaopoeder.
