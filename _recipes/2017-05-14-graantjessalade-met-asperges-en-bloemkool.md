---
layout: recipe
title: Graantjessalade met asperges en bloemkool
favorite: false
show: true
source: Lekker van bij ons
source_url: https://www.lekkervanbijons.be/recepten/graantjessalade-met-asperges-en-bloemkool
duration: "35 min"
tags: [groenten, slaatje]
---

### Ingrediënten

* 1 bussel witte asperges
* 1/2 bloemkool
* 100 g rucola
* 200 g farro (of spelt, rijst, bulgur, couscous, quinoa,...)
* 30 g rozijnen
* beetje zonnebloemolie
* 2 el rodewijnazijn
* 1 el maisolie
* snuif peper en zout
* 30 g walnoten

### Bereiding 

Blancheer of stoom de bloemkool en geschilde asperges. Kook de graantjes gaar in gezouten water, volgens de aanwijzingen op de verpakking. Laat afkoelen.

Bak de bloemkoolroosjes en aspergepunten kort in de pan met wat zonnebloemolie. Kruid met peper en zout.

Hak de walnoten en de rucola grof. Meng de rozijnen, helft van de walnoten, rucola, de bloemkoolroosjes en asperges door de graantjes. Sprenkel er de maïsolie en rodewijnazijn over. Kruid met peper en zout en hussel voorzichtig door elkaar. Werk af met de rest van de walnoten.