---
layout: recipe
title: VEGAN PARMEZAANSE KAAS
favorite: false
show: true
source: Wat eet je dan wel
source_url: http://wateetjedanwel.nl/vegan-parmezaansekaas/
duration: "5 min"
tags: [kaas, veggi, vegan]
---


### Ingrediënten

* 4 gram knoflookpoeder
* 6 gram zeezout
* 15 gram edelgistvlokken
* 150 gram ongezouten cashewnoten

### Bereiding 

Maal alle ingrediënten in een keukenmachine. Bewaar in een goed afsluitbare pot.