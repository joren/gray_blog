---
layout: recipe
title: Gebakken pladijs en tarwerisotto met groene asperges
favorite: false
show: true
source: Dagelijkse kost
source_url: http://www.een.be/programmas/dagelijkse-kost/recepten/gebakken-pladijs-en-tarwerisotto-met-groene-asperges?&personen=2
duration: "30 min"
tags: []
---

### Bereiding 

De tarwerisotto met asperges 
Pel de ui en snipper hem fijn. Fruit de stukjes glazig in een flinke scheut olijfolie.

Pel de knoflook en plet de tenen tot pulp. Doe hem bij de ui en laat alles glazig worden. Voeg eventueel nog wat olijfolie toe.

TIP: Niet zo dol op knoflook? Gebruik dan 1 teentje of laat het ingrediënt weg.

Strooi de tarwekorrels in de pan en roer goed om zodat alle korrels met een laagje olie bedekt zijn.

Giet er de warme bouillon bij, zet een deksel op de pan en laat de tarwe 15 tot 20 minuten garen op een zacht vuur.

TIP: Normaal gezien nemen de korrels al het kookwater op. Als er aan het einde van de kooktijd toch nog water overblijft, giet je de risotto af.

Snijd de vezelige achterkanten van de asperges en verwijder de schil van het onderste deel van de stengels. Dunne groene asperges hoef je niet te schillen.

Snijd ze in stukjes van ongeveer 2 cm. Kook de asperges in enkele minuten beetgaar in licht gezouten water. Giet af en laat uitlekken.

Was de peterselie, dille en dragon. Verwijder taaie steeltjes en snipper alle groene kruiden fijn.

Doe de flinke klont boter in de hete tarwerisotto en rasp er Parmezaanse kaas bij. Roer tot beide weggesmolten zijn. Proef en voeg naar smaak extra kaas toe.

Schep er de stukjes groene asperge en de tuinkruiden door. Breng indien nodig op smaak met peper en wat zout.

De pladijzen 
Smelt een flinke klont boter in de pan en laat ze een beetje bruinen tot ze de kleur van hazelnoot heeft.

Kruid het visvlees met een beetje zout en gemalen peper.

Leg de pladijsfilets op de velkant in de pan. Druk ze eventueel met een paletmes aan, zodat ze niet opkrullen en gelijkmatig kunnen bakken.

Zet het vuur zacht en laat de vis rustig garen van onder naar boven. Draai de vis pas om als hij precies voldoende gebakken is en laat de bovenzijde nog kort kleuren.

Om af te werken 
Pluk de fijnste takjes waterkers los en spoel ze voorzichtig in water. Laat het groen uitlekken.

Schep porties tarwerisotto in diepe borden.

Schik de pladijsfilets op de risotto en strooi er wat waterkers over.

Druppel citroensap en een klein beetje fijne olijfolie over de bereiding en serveer meteen.