---
layout: recipe
title: De simpelste vegan pannenkoeken ooit
favorite: true
show: true
source: EVA
source_url: https://www.evavzw.be/recept/de-simpelste-vegan-pannenkoeken-ooit
duration: "20 min"
tags: [vegan, ontbijt, dessert]
---

### Ingrediënten
* 1 tl vanille extract 
* 2 el agavesiroop of ahornsiroop
* 2 el neutrale olie
* 250 ml plantaardige melk (amandel of soja)
* 1/4 tl zout
* 1 tl bakpoeder
* 150 g bloem

### Bereiding 

Meng de droge ingrediënten (bloem, bakpoeder en zout) in een kom. Meng de natte ingrediënten (melk, olie, agavesiroop en vanille-extract) in een andere kom tot het geheel goed gemengd is.

Voeg het natte mengsel toe aan de droge ingrediënten en roer door elkaar. (Er mogen kleine 'klontjes' overblijven.)

Laat het deeg een vijftal minuten rusten.

Verwarm intussen een pannenkoekenpan met een beetje vetstof op middelhoog vuur.

Bak drie à vier pannenkoekjes tegelijk, afhankelijk van de grootte van je pan. Deze 'American pancakes' zijn kleiner en dikker dan de Vlaamse pannenkoeken.

Serveer met toppings naar keuze. Bv. vers fruit, chocolade, poedersuiker, ahornsiroop...
