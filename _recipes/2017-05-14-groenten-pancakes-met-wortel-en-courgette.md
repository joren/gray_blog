---
layout: recipe
title: Groenten pancakes met wortel en courgette
favorite: false
show: true
source: Lekker van bij ons
source_url: https://www.lekkervanbijons.be/recepten/groenten-pancakes-met-wortel-en-courgette
duration: "30 min"
tags: [brunch, groenten, ontbijt]
---


### Ingrediënten

* 1 grote courgette
* 100 g geraspte wortelen
* 1 bosje lente ui
* 3 teentjes look
* 3 el gehakte peterselie
* 200 g zelfrijzende bloem
* 3 eieren
* 300 ml melk
* 1 el bakpoeder
* snuif zout en peper

### Bereiding 

Rasp de courgette, laat het meeste vocht goed uitlekken en leg in een vergiet. Probeer zoveel mogelijk vocht van de courgette er uit te knijpen/duwen.
Snijd de lente-ui in fijne ringen. Pers de knoflook boven de kom met geraspte groenten.

Maak het beslag, klop de eieren luchtig en voeg afwisselend melk en bloem toe. Voeg tenslotte het bakpoeder en het peper & zout toe. 
Meng het deeg samen met de courgette, wortel, lente-uitjes en de look. 
Bak er kleine pancakes van, wacht tot het deeg gestold is alvorens te draaien.