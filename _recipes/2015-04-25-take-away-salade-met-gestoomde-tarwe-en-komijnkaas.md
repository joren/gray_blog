---
layout: recipe
title: "Take-away salade met gestoomde tarwe en komijnkaas"
favorite: false
show: true
source: Dagelijkse kost
source_url: "http://www.een.be/programmas/dagelijkse-kost/recepten/take-away-salade-met-gestoomde-tarwe-en-komijnkaas?&personen=2"
duration: "30 min"
tags: [veggi, salade]
---

### Ingrediënten

#### Voor de groentesalade met tarwe
* 200g voorgestoomde tarwekorrels (bv. Ebly)
* 3dl groente- of kippenbouillon (evt. water)
* 150g kerstomaten of mini-trostomaten)
* 150g rammenas ('zwarte radijs')
* 150g komijnkaas (bv. 'Herbie')
* 50g rucola
* 2 lente-uitjes
* 1/2 komkommer
* 1 takje dragon
* enkele takjes koriander
* snuif peper
* snuif zout

#### Voor de dressing
* 150g zure room
* 1el graanmosterd
* 1el honing
* 1el witte wijnazijn
* 50g walnoten
* snuif peper
* snuif zout

### Bereiding

### De gestoomde tarwe voorbereiden
Bereid of ontdooi vooraf verse groente- of kippenbouillon. Je kan de tarwekorrels ook koken in water met een snuif zout.

Meng de voorgestoomde tarwe met een juiste hoeveelheid bouillon. Breng het mengsel aan de kook op een zacht vuur. Gaar de tarwe zoals aangegeven door de producent. (doorgaans ong. 10 minuten)

Schep de korrels in een grote zeef en koel ze af onder een straal koud water.

### De dressing
Doe de zure room in een schaal en meng dit met de graanmosterd en honing en de azijn. Kruid de dressing met naar smaak met peper en zout.

Hak de walnoten in matig fijne stukken en roer ze door de romige saus.

### De salade bereiden
Maak de lente-uitjes schoon en snij ze in fijne ringen.

Snij de (ongeschilde) komkommer overlangs in kwarten. Verwijder de waterige kern met zaden. Snij het vruchtvlees vervolgens in kleine gelijke blokjes van een halve centimeter breed.

Snij de kleine tomaten in kwartjes. Hele kleine exemplaren kan je op z'n geheel laten.

Schil de rammenas en rasp het witte vruchtvlees met een grove groenterasp.

Verwijder de korst van de dikke plak komijnkaas. Snij de kaas in gelijke dobbelstenen.

Meng de fijne groenten met de tarwe en de kaasblokjes. Voeg een beetje zout en versgemalen peper toe.

Spoel de blaadjes rucola met koud water en laat de notensla uitlekken.

Rits de blaadjes dragon los en pluk blaadjes koriander.

### je lunchbox vullen of de borden presenteren 
Tip: Roer de dressing niet vooraf door de salade. Vul je lunchbox eerst met een bodem van de dressing en schep de mengeling van tarwe, groenten en kruiden erbovenop. Meng alles net voor je aan de maaltijd begint.

Lepel onderaan een bodem van de dressing. Verdeel daarover een portie rucola, en enkele blaadjes dragon en koriander.

Schep de salade van gestoomde tarwe, groenten en kaasblokjes erbovenop.

Meng alles alvorens je toehapt.
