---
layout: recipe
title: Vegetarische lasagne zonder pasta
favorite: false
show: true
source: Lekker van bij ons
source_url: https://www.lekkervanbijons.be/recepten/vegetarische-lasagne-zonder-pasta
duration: "60 min"
tags: [groenten, veggi]
---

### Ingrediënten


#### TOMATENSAUS
* 2 uien
* 2 teentjes look
* 1 rode chilipeper zonder zaadjes
* 1 dl wijn
* 400 g tomatenblokjes
* 500 g tomatepulp
* 70 g tomatenpuree
* 2 blaadjes laurier
* 1 takje tijm

#### GEGRILDE GROENTEN
* 2 aubergines
* 2 courgettes
* 300 g verse spinazie

#### AFWERKING
* 400 g ricotta
* 100 g parmezaanse kaas
* 1 bolletje mozzarella
* snuif peper en zout
* beetje olijfolie

### Bereiding 

Snijd de aubergine en courgette in plakjes. Grill ze aan beide kanten op een grillpan. Zet even aan de kant.

Maak de saus. Doe een scheutje olijfolie in een stoofpot. Stoof de gesnipperde uien, geplette look en chilipeper tot de ui glazig is. Schep er de geconcentreerde tomatenpuree bij en laat kort meestoven. Doe er de tomatenblokjes, de laurier en tijm bij. Schenk er een scheut witte wijn bij en laat de alcohol verdampen. Voeg dan de tomatenpulp toe. Laat de saus minimum een halfuurtje pruttelen op een zacht vuurtje.

Verwarm de oven voor op 200°C. Roerbak de spinazie kort en kruid met peper en zout. Meng met de ricotta.

Vet een ovenschotel in. Leg afwisselend courgetteplakjes, tomatensaus, aubergineplakjes met de ricottaspinazie. Herhaal tot de schotel gevuld is.

Snijd de mozzarella in plakjes en leg ze bovenop de groentelasagne. Werk af met de Parmezaanse kaas. Zet gedurende 30 min. in de voorverwarmde oven. Serveer met stokbrood. 