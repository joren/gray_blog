---
layout: recipe
title: 'Champagne gratiné'
favorite: false
show: true
source: Ellen
duration: "20 min"
tags: [dessert]
---

### Bereiding 

Smelt 50g suiker met 5cl water in een ruime pan, tot een siroop. Dat is ongeveer een volle vijf minuten doorkoken, tot het water stroperiger is. Dan voeg je daar 10cl cava, champagne, ... Aan toe, van het vuur. Dat hoeft niet meer te koken. Tot slot voeg je de zeste van een halve bio-citroen toe. Goed mengen, invriezen, 24u later klaar.
