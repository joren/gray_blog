---
layout: recipe
title: Cous-Cous á la Jøren
favorite: false
show: true
source: Joren
duration: "15 min"
tags: [cous-cous, veggi]
---

### Ingrediënten

#### Cous Cous
* 125g cous-cous
* 2 tomaten
* 1 paprika (kleur te kiezen)
* 1 ajuin
* 2 wortels
* 1 teentje look
* 1 rode peper
* 125g worst of veggi alternatief 
* handvol zoute nootjes
* handvol rozijnen
* 1 blik opgelegde kikkererwten
* 1 el ras el hanout
* handvol verse munt
* handvol verse koriander

### Bereiding 

### De couscous

1. Doe de couscous in een kom die tegen hitte kan 
2. Meng wat nootjes en rozijntjes tussen de couscous. 
3. Meng hier ook een stevige lepel ras el hanout kruiden onder 
4. Giet kokend water over de couscous tot deze net onder water staat en dek af met een handdoek

Dit mag je nu negeren tot wanneer je alles samen gooit

### De groentenmengeling

1. Zet een grote kom op het vuur met olijfolie 
2. Voeg de ajuin en look toe en laat even stoven 
3. Voeg kleine blokjes wortel toe 
4. Voeg de blokjes paprika toe 
5. Voeg de peper toe (zo groot of klein als je zelf wilt) 
6. Voeg de kikkererwten toe (zonder vocht) 
7. Voeg eventueel een beetje kokend water toe als je schrik hebt dat het gaat aanbranden 
8. Voeg hier eventueel nog wat nootjes, rozijnen en ras el hanout aan toe 
9. laat even stoven tot alles gaar is 
10. Bak de vegi-blokjes of stukjes worst en voeg toe aan de mengeling wanneer je het vuur afzet.

## Le magic

Meng, al dan niet in een andere kom, de couscous met de groentenmengeling.

Schep op en voeg per bord een halve tomaat toe in blokjes en fijngehakte munt en verse koriander.

Smakelijk