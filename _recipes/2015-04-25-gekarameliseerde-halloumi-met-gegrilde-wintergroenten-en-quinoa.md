---
layout: recipe
title: Gekarameliseerde Halloumi met gegrilde wintergroenten en quinoa
favorite: false
show: true
source: Dagelijkse kost
source_url: http://www.een.be/programmas/dagelijkse-kost/recepten/gekarameliseerde-halloumi-met-gegrilde-wintergroenten-en-quinoa?&personen=2
duration: "35 min"
tags: [grill, halloumi, quinoa, veggi]
---

### Ingrediënten

#### Voor de gebakken halloumi
* 250g halloumi
* 1 1/2 eetlepels vloeibare honing
* snuif (gedroogde) tijm
* snuif gedroogde oregano
* fsnui peper
* snuif zout

#### Voor de gegrilde groenten
* 2 rode biet
* 1/2 butternut
* 2 ui
* snuif tijm
* snuif oregano
* scheutje balsamico
* scheutje honing
* scheutje olijolie

#### Voor de quinoa
* 100g quinoa
* 2 1/2dl groentebouillon

#### Afwerken en serveren
* 1/2 bakje tuinkers
* enkele blaadjes bloedzuring (eventueel)
* scheutje balsamico

### Bereiding 

### voorbereiding 
* Kook de rode bieten gaar in water. Reken hiervoor een uur. Laat ze wat afkoelen en schil ze. 

### De bereiding 
* Verhit de grillpan op een hoog vuur en geef ze voldoende tijd om bloedheet te worden.

* Schil de butternut (of een stuk pompoen) en verwijder de zaadjes in het midden van de vrucht. Snij het vruchtvlees in brute hapklare stukken van enkele centimeters breed. Druppel er een beetje olijfolie overheen.

* Snij de rode bieten in partjes en druppel er ook een klein beetje olijfolie over. Grill ze langs de beide zijden in de hete pan en zet ze opzij.

* Opmerking: Snij de bieten op een bord, zodat het felgekleude sap je werkplank niet vlekt. Gebruik zeer weinig olie. Zo vermijd je dat de vlam in de pan slaat.

* Grill aansluitend de stukken butternut tot er een streepjespatroon in gebrand is.

* Schil de uien en verdeel de bollen in partjes. Ook hier druppel je een zuinig scheutje olijfolie over en je verhuist ze naar de grill.

* Verwarm de oven voor tot 200°C.

* Verzamel alle gegrilde groenten in een ruime ovenschaal. Leg de stukken biet opzij, zodat ze de andere groenten niet verkleuren.

* Druppel een royale scheut olijfolie over de groenten, gevolgd door een laagje vloeibare honing en wat balsamico.

* Kruid de groenten met een snuif gedroogde (of verse) tijm en wat oregano.

* Gaar ze gedurende 15 minuten in de hete oven van 200°C. (Prik in de pompoen ter controle.)

* Ontdooi en verwarm groentebouillon. Je kan ook zelf groentebouillon maken.

* TIP: Je kan ook een snelle bouillon koken op basis van water met de smaken die je in het gerecht verwerkt of die erbij aansluiten. Bv. takjes verse tijm, rozemarijn, oregano…

* Doe 1 deel quinoa en 2 delen bouiilon in een kookpot. Laat de zaadjes garen op een zacht vuur, onder deksel. (Respecteer de kooktijd die de verpakking vermeldt.)

* Snij de halloumi in plakken van een centimeter dik.

* Schenk een flinke scheut olijfolie in een mengkom. Voeg er de honing aan toe, samen met een snuif gedroogde tijm en oregano. Voeg ook voldoende versgemalen peper toe en een beetje zout. Meng alles met de garde.

* Verhit een scheutje olijfolie in een braadpan op een matig vuur. Bak de plakken haloumi tot ze langs de beide zijden wat kleur hebben.

* Schenk vervolgens het mengsel met honing en kruiden in de pan. Laat de halloumi enkele minuten pruttelen op een zacht vuur, zodat de kaas licht karameliseert.

* (Let erop dat de honing niet verbrandt.)

### Afwerken en serveren 
* Lepel de gare quinoa in een ruime schaal. Schep er de gegrilde ovengroenten bovenop. Druppel ook het braadvocht van de groenten over het gerecht.

* Werk het veggie gerecht af met plakken gekarameliseerde halloumi, wat van de honingsaus, plukken tuinkers en druppels balsamico. Wie bloedzuring kon kopen: enkele elegante blaadjes geven de bereiding extra ‘kick’.