---
layout: recipe
title: BULGURSALADE MET GROENTEN EN GRANAATAPPEL
favorite: false
show: true
source: Dagelijkse kost
source_url: http://www.een.be/programmas/dagelijkse-kost/recepten/bulgursalade-met-groenten-en-granaatappel?&personen=2
duration: "30 min"
tags: [bulgur, veggi]
---

### Ingrediënten

#### Voor de salade
* 200g bulgur
* 4dl groentenbouillon
* 1 rije avocado
* 1/2 komkommer
* 3 lente-uitjes
* 1/2 granaatappel
* 1/4 citroen
* enkele takjes munt
* enkele takjes koriander
* 50g zoute geroosterde pindanoten
* snuifje peper
* snuifje grof zeezout of fleur de sel
* snuifje zout
* scheutje olijfolie
* snuifje komijnpoeder
* snuifje gerookt paprikapoeder

#### Voor de dressing
* 100g platte kaas
* 15g gember
* 1 eetlepel vloeibare honing
* scheutje olijfolie

### Bereiding 

# De bulgursalade

1. Ontdooi en verwarm de groentebouillon of start met versgemaakte bouillon.

2. Doe de bulgur in en pot en schenk er hete bouillon over. Breng de tarwekorrels zacht aan de kook en laat ze de bouillon opslorpen. Zodra de bulgur gaar is stort je de korrels in een zeef of vergiet en spoel je ze koud onder een straal water.

3. Snij de granaatappel in twee en klop met een vijzelstamper of een gelijkaardig voorwerp op de halve vruchten. Zo haal je er de zaden makkelijk uit. Vang ze op een een ruime schaal.

4. Snij de uiteinden van de komkommer en snij de vrucht overlangs in twee. Lepel de vochtige zaadlijsten eruit en snij het vruchtvlees in kleine blokjes.

5. Maak de lenteuitjes schoon en snij ze in dunne ringen.

6. Snij de rijpe avocado’s in twee, verwijder de grote pit en verdeel de ze in hapklare partjes. Trek de dikke schil eraf.

7. Snij de kerstomaten middendoor.

8. Stort de afgekoelde bulgur in en ruime schaal. Druppel er vers citroensap overheen en schenk er wat fijne olijfolie over. Voeg versgemalen peper toe, een snuifje komijnpoeder en wat zout. Meng en proef of de smaken goed zitten.

9. Strooi de blokjes komkommer en de ringen lenteui erover. Kruid de partjes avocado met een bescheiden snuif gerookt paprikapoeder en wat grof zeezout. Druppel er ook een beetje fijne olijfolie over. Verdeel ze samen met de halve kerstomaten over de salade.

10. Strooi tenslotte de sappige zaden van de granaatappel over de bereiding.

11. Schep de plattekaas in en schaaltje en voeg honing en fijne olijfolie toe. Schil het stukje gemberknol en rasp het bij de plattekaas. Meng alles tot een fris sausje.

12. Spoel de takjes koriander en munt en pluk de blaadjes los. Verdeel het groen ongesneden of grofgesneden over de salade.

13. Lepel wat van de dressing over de bereiding en presenteer de rest apart.

14. Hak de zoute pindanoten grof, strooi ze over de bulgursalade en serveer.