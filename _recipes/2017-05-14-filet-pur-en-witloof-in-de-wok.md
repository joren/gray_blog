---
layout: recipe
title: Filet pur en witloof in de wok
favorite: false
show: true
source: Lekker van bij ons
source_url: https://www.lekkervanbijons.be/recepten/filet-pur-en-witloof-in-de-wok
duration: "20 min"
tags: [wok, groenten]
---

### Ingrediënten

* 4 stronkjes witloof
* 1 rode paprike
* 1 groene paprika
* 2 perziken in siroop
* 1 ui
* 300g file pur
* 1/2 kl zout
* 1 kl suiker
* 1/4 kl witte peper
* 2 kl sesamolie
* 3 kl vloeibare rundboillon
* 2 kl aardappelzetmeel
* genoeg slaolie (om te wokken)

### Bereiding 

Snijd de stronkjes witloof in 4. 
Snijd de filet pur in reepjes. 
Snijd de paprika, perzik en ui eveneens in reepjes.

Verhit de olie en bak de reepjes filet pur halfgaar. 
Leg het vlees op een bordje. 
Verhit de olie, bak de ui goudbruin en voeg er nadien de witloof, paprika en perzik aan toe en wok gedurende ongeveer 2 minuten. 
Voeg het vlees toe, zout, peper, suiker, sesamolie en bouillon.

Meng het aardappelzetmeel met koud water en voeg toe om de saus in te dikken.

Serveertip: Dien op met geparfumeerde, gestoomde rijst. Versier het bord met vers seizoensfruit of schijfjes komkommer of sinaasappel.