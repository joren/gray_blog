---
layout: recipe
title: 'Pasta / courgette / geitenkaas / munt'
favorite: true
show: true
source: Au Bain-Marieke
source_url: http://aubainmarieke.be/pasta-courgette-geitenkaas-munt/
duration: "15 min"
tags: [pasta, veggi]
size: 2 personen
---

### Ingrediënten

* 200g Capellini
* 1 courgette
* bussel verse munt
* 1 rolletje geitenkaas (lopende, geen droge)
* snuif zwarte peper

### Bereiding

Zet gezouten water op voor je pasta en kook de pasta volgens de instructies op de verpakking
Was de courgette en snij in dunne repen (eventueel met een dunschiller)
Snij de geitenkaas in plakjes
Snij de munt in fijne reepjes
Gril/bak de courgette in wat olijfolie en kruid met peper en zout
Giet je pasta af, meng met de gegrilde courgetteslierten en munt en kruid bij met peper en zout naar smaak.
Werk af met de geitenkaas en eventueel nog wat extra munt en olijfolie
Klaar! :)
