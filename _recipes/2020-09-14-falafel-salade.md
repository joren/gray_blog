---
layout: recipe
title: Frisse falafel salade
favorite: false
show: true
source: followfitgirls
source_url: https://www.followfitgirls.com/falafel-salade
duration: "20 min"
tags: [vegan, salade, falafel]
size: 4 personen
---

### Ingrediënten

* 300 gram veldsla
* 4 stuks volkoren tortilla’s
* 200 gram volkoren couscous
* 400 gram falafel
* 4 stuks tomaten
* 1 stuk komkommer
* Olijfolie
* Marokaanse Ras el Hanout
* Peper
* Zout

Optioneel kun je nog gedroogde cranberry’s en rozijnen toevoegen.

#### Dressing

* 200 ml soja yoghurt, of andere plantaardige yoghurt
* Citroen
* Zoetstof of Stevia

### Bereiding

* Verwarm de oven voor op 180 graden. Of wat staat aangegeven op de verpakking van de tortilla’s
* Snijd de komkommer in schijfjes en de tomaat in blokjes
* Oven opgewarmd? Snijd iedere tortilla in 8 pizzapunten. Leg voor 6 minuten in de oven totdat ze goudbruin en knapperig zijn. Tada, you made yourself some tortillachips!
* Bak de falafel ongeveer in 5 tot 6 minuten goudbruin in een pan met olijfolie
* Bereid de couscous volgens de verpakking
* Voeg vervolgens de komkommer en de tomatenblokjes toe aan de couscous en meng
* Leg de veldsla in een kom, bedek met het couscousmengsel. Verdeel de falafel over de kom en breng op smaak met zout en peper. Voeg naar eigen smaak de Ras el Hanout kruiden toe. Ik doe per persoon ongeveer een halve eetlepel. Let op; dit is vrij kruidig
* Steek de tortillachips langs de zijkanten in de kom
* Meng in een bakje de (soja) yoghurt met het sap van een citroen ( naar smaak ) en voeg een beetje zoetstof toe. De dressing moet fris met een vleugje zoet zijn als tegenhanger van de Ras El Hanout kruiden.
* Besprenkel vlak voor het serveren het gerecht met de yoghurtdressing. Voeg eventueel cranberry’s en rozijnen toe; and here you go!
