---
layout: recipe
title: Salade van avocado, tomaat en paprika
favorite: false
show: true
source: All Recipes
source_url: http://allrecipes.nl/recept/795/salade-van-avocado--tomaat-en-paprika.aspx
duration: "15 min"
tags: [veggi, salade]
---

### Ingrediënten

#### Salade
* 2 avocado's
* 1 rode ui
* 1 groene paprika
* 2 tomaten
* een handvol verse koriander
* 1/2 limoen
* snuif peper en zout

#### Optioneel
* 1 ciabatta 
* 1 el sesamzaad

### Bereiding 

1. In een middelgrote kom avocado, ui, paprika, tomaat, koriander en limoensap goed omscheppen. Op smaak brengen met zout en peper. 
2. Werk eventueel af met wat gegrilde sesamzaadjes