---
layout: recipe
title: Rabarberschuimtaart
favorite: true
show: true
source: Schoonmoeder
duration: "40 min"
tags: [cake, dessert]
---

### Ingrediënten

#### Ingredienten cake
* 1 eierdooier
* 2 eireren
* 1p vanillesuiker
* 150g boter
* 1 1/2 tlp bakpoeder
* 75g aardappelbloem
* 150g zelfrijzende bloem
* 600g rabarber

#### Schuim
* 1 tlp citroensap
* 150g suiker
* snuif zout
* 3 eiwitten

### Bereiding 

### cake

1. Boter zacht en schuimig roeren met de suiker en vanillesuiker 
2. Eieren en dooier toevoegen 
3. bloemenmengsel toevoegen 
4. In een beboterde en met bloem bestrooide springvorm doen en de in stukjes gesneden rauwe rabarber erop leggen 
5. bakken in een voorverwarmde oven op 180 graden gedurende 25min

### Schuim 
1. De eiwitten stijfkloppen met het zout, suiker en het citroensap 
2. Cake uit de oven halen, eiwitschuim erop doen, met de achterzijde van een lepel golfjes maken en opnieuw bakken gedurende ongeveer 25min op 150graden (het eitwit moet lichtbruin zien.