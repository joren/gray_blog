---
layout: recipe
title: Roze chil­led chee­se­ca­ke
favorite: false
show: true
source: Albert Hein
source_url: http://www.ah.be/allerhande/recept/R-R804324/chilled-cheesecake
duration: "30 min"
tags: [oreo, cake, dessert]
---

### Ingrediënten

* 30 g boter
* 154 g Oreo-koekjes
* 3 blaadjes gelatine
* 375 g Frambozen
* 8 g vanillesuiker
* 125 ml slagroom
* 4 el suiker
* 250 g mascarpone
* 4 el bietensap

### Bereiding 

1. Bekleed de bodem van de springvorm met bakpapier. Smelt de boter in een steelpan. Maal de koekjes fijn in de keukenmachine en voeg de boter toe. Bedek de bodem van de vorm ermee, druk aan en zet in de koelkast. 
2. Week de gelatine 5 min. in koud water. Pureer ondertussen de frambozen met de vanillesuiker met de staafmixer. Verwarm een deel van de frambozenpuree in een steelpan tegen de kook aan. Neem van het vuur. Knijp de gelatine uit en los op in de warme puree. Voeg toe aan de koude puree. 
3. Klop de slagroom stijf met de suiker. Roer de crème fraiche en mascarpone los in een beslagkom. Spatel de frambozenpuree, slagroom en het bietensap er luchtig door. Schep het op de taartbodem. Zet minimaal 3 uur afgedekt in de koelkast.