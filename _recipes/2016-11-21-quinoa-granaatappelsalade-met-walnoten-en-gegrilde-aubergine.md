---
layout: recipe
title: Quinoa-granaatappelsalade met walnoten en gegrilde aubergine
favorite: false
show: true
source: Marley Spoon
source_url: https://marleyspoon.be/menu/6624-quinoa-granaatappelsalade-met-walnoten-en-gegrilde-aubergine
duration: "30 min"
tags: [aubergine, quinoa, veggi]
---

### Ingrediënten

* 1 granaatappel
* teen knoflook
* snuif zout en peper
* 1 olijfolie
* 1 azijn
* 1 granaatappelsiroop of honing
* 1 verse munt
* 1 walnoten
* 1 yoghurt
* snuif baharat kruiden
* 1 Aubergine
* 250gr Quinoa

### Bereiding 

### Aubergine voorbereiden

Grill op 250°C voorverwarmen. Aubergines over de lengte in ca. 1cm dikke schijven snijden. Bakplaat met bakpapier bedekken en daarover de aubergine-schijven verdelen. Dan baharat kruiden met een flinke snuf zout over aubergine verdelen. Als je niet van kruidig houdt deel van baharat gebruiken.

### Aubergine grillen

Vervolgens aubergine met 2-3el olie besprenkelen en voor ca. 15-20min in oven grillen totdat deze goudbruin gekleurd zijn. Na 8-10min eenmaal omdraaien om beide kanten goed te garen.

### Quinoa koken

Ondertussen in middelgrote kookpan op hoog vuur ca. 500ml water aan de kook brengen met snuf zout. Zodra water kookt quinoa toevoegen en op laag vuur in ca. 15min beetgaar koken. Dan de quinoa in zeef uit laten lekken.

### Dressing bereiden

Ondertussen blaadjes van munt plukken en grof hakken. Knoflook pellen en fijn raspen of hakken. Walnoten grof hakken. In een kom granaatappelsiroop, 2el azijn, 2el olie, helft van munt, helft van walnoten en helft van knoflook vermengen. Vervolgens met zout en peper op smaak brengen en naar smaak meer knoflook toevoegen.

### Granaatappel voorbereiden

Middelgrote kom met water vullen. Granaatappel halveren en in water openbreken. Huid en vruchtwanden verwijderen. Drijvende deeltjes verwijderen, vervolgens water afgieten en granaatappelpitten in zeef opvangen.

### Salade serveren

Quinoa met dressing vermengen en over borden verdelen. Daarover aubergine verdelen en naar smaak met yoghurt besprenkelen. Tot slot met rest van walnoten, munt en granaatappelpitten garneren.