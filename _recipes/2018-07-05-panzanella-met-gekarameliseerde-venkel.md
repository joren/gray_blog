---
layout: recipe
title: Panzanella met gekarameliseerde venkel
favorite: false
show: true
source: EVA
source_url: "https://www.evavzw.be/recept/panzanella-met-gekarameliseerde-venkel"
duration: "30 min"
tags: [veggi, salade, vegan]
---

### Ingrediënten

#### Panzanella
* handvol basilicum
* 2 dikke sneden stevig brood van minstens een dag oud
* 1 teentje look
* 2 tl gezouten kappertjes (goed afgespoeld en uitgelekt)
* handvol zwarte ontpitte olijven
* 1 grote venkel
* 7 trostomaten

#### Dressing
* 1 tl ahornsiroop
* 2 el olijfolie
* 1 el balsamicoazijn

### Bereiding 

1. Pel de tomaten, verwijder de kroontjes en snijd ze, afhankelijk van de grootte, in vier of zes stukken. Doe ze in een grote kom. 
2. Doe vervolgens de olijven en de kappertjes bij de tomaten. Gebruik je een teentje look? Pers het dan uit over het tomatenmengsel. 
3. Klop de ingrediënten voor de dressing krachtig door elkaar en schenk ze over de tomaten. Roer alles door elkaar. 
4. Verwijder dan de voet en de harde stelen van de venkel en schil de buitenste vezels weg met een dunschiller. Snijd de knol overlangs in dunne plakken. Verwarm wat olijfolie in de pan en bak de venkelplakjes aan beide kanten tot ze goudbruin kleuren, heel zacht zijn en enigszins karameliseren.. 
5. Schep de venkelplakjes op een bord en laat ze een beetje afkoelen. 
6. Snijd de sneden brood nu in blokjes. Verwarm opnieuw wat olijfolie in de pan en bak de broodblokjes aan beide kanten goudbruin. Hou dit goed in de gaten, want brood verbrandt snel! 
7. Snijd de gekarameliseerde venkelplakjes in reepjes en hussel ze samen met de broodkorstjes door de tomatensalade. Scheur er de basilicumblaadjes over en bestrooi met versgemalen peper en zout naar smaak. Let op: de kappertjes zijn ook al zout, dus proef voor je er te veel zout aan toevoegt. 
8. Serveer de salade lauwwarm of op kamertemperatuur. Koud komen de smaken niet zo goed tot hun recht.
