---
layout: recipe
title: Portobello gevuld met couscoussalade van bloemkool en broccoli
favorite: false
show: true
source: Lekker van bij ons
source_url: https://www.lekkervanbijons.be/recepten/portobello-gevuld-met-couscoussalade-van-bloemkool-en-broccoli
duration: "35 min"
tags: [groenten, veggi]
---


### Ingrediënten

* 4 portobello champignons
* 1/2 bloemkool
* 1 broccoli
* 2 el zonnebloempitten
* 1 el lijnzaad
* 2 el pompoenpitten
* 2 el rozijnen
* 2 el gedroogde cranberries
* 2 el walnoten
* 1 citroen sap
* beetje maisolie
* snuif peper en zout
* 4 lente uitjes
* 150 g hummus

### Bereiding 

Hak de bloemkool en broccoli fijn in een hakmolen of blender. Rooster de zonnebloempitten, walnoten, pompoenpitten en lijnzaad in een pan zonder vetstof.

Meng de geroosterde zaden door de couscous van bloemkool en broccoli. Voeg de rozijnen en gedroogde cranberries toe. Breng op smaak met citroensap en maïsolie. Kruid met peper en zout.

Verwarm de oven voor op 180°C. Bestrijk de portobello’s en de lente-uitjes met olie. Bak de lente-uitjes op de grillpan en bak de portobello’s krokant in de oven. Kruid met peper en zout.

Vul de portobello’s met de couscous en werk af met de gegrilde lente-uitjes. Lekker met hummus.