---
layout: recipe
title: Tajine van kip met citroen en olijven
favorite: false
show: true
source: Dagelijkse kost
source_url: http://www.een.be/programmas/dagelijkse-kost/recepten/tajine-van-kip-met-citroen-en-olijven?&personen=2
duration: "45 min"
tags: [tajine, kip]
---

### Ingrediënten

#### De marinade
* 1/2 el korianderzaad
* 1 citroen
* 1/2 teentje look
* 1 eetlepels ahornsiroop (of een beetje honing)
* 1/2 stuk gember
* 5 cl olijolie
* snuif peper en zout
* 2 kippenbouten

#### De Tajine
* 1 1/2 uien
* 6 kleine aardappelen
* 2 takjes verse rozemarijn
* 7 1/2 cl witte wijn

#### Afwerking
* 1/2 citroen
* 1/8 bussel platten peterselie
* 75 g zwarte olijven (ontpit)

### Bereiding 

### vooraf: 
* Traditionele tajines in aardewerk kan je al krijgen vanaf ongeveer €20. Er zijn ook duurdere versies op de markt, aangepast aan de moderne keukentoestellen. 

* Een traditionele tajine in aardewerk kan je niet gebruiken op een inductiekookplaat. 

* Als je een traditionele tajine voor de eerste keer gebruikt, dompel de stoofpot dan één nacht onder in water en smeer nadien de binnenkant van de schaal en het deksel in met olie (bv. zonnebloemolie). Warm je hem voor het eerst na het weken opnieuw op, dan zal hij even flink staan dampen. Geen paniek: dat is perfect normaal.

### de marinade: 
* Marineer de kip minstens een uurtje vooraf. (Langer mag ook.) 

* Strooi het korianderzaad in de vijzel en stamp de graantjes fijn. (Je kan de koriander vooraf ook nog even kort roosteren in een hete pan.) Doe de fijngestampte koriander in een ruime mengschaal. 

* Pel de look en plet ze tot pulp. Schil de gember en rasp hem. Doe alles in de mengschaal. 

* Voeg nu het verse citroensap toe, de zoete ahornsiroop en een flinke scheut olijfolie. 

* Kruid de marinade met wat peper van de molen en een snuif zout. 

* Verdeel de kippenbouten in het heupstuk en de drumstick. Dat doe je door met een scherp mes het gewricht van elke bout door te snijden. (Je kan natuurlijk ook aan de poelier vragen om dat vooraf even te doen.) 

* Wentel de stukken vlees in de marinade, dek de pot af met wat vershoudfolie of een deksel en laat alles minstens een uurtje verder marineren in de koelkast.

### de tajine: 
* Spoel de aardappelen en snij ze ongeschild in hapklare stukken. 

* Pel de uien en snij ze grof. 

* Plaats de tajine op een matig vuur en laat ze heet worden. 

* Laat de gemarineerde stukken kip even uitlekken en hou de marinade bij. 

* Verhit een scheutje olijfolie in de schaal van de tajine en bak de stukken kip kort, tot ze een kleurtje krijgen. Het vlees hoeft niet gaar te zijn. Schep de kip nadien uit de tajine. 

* Zet het vuur wat zachter en doe nu de stukjes ui en aardappel in de tajine. Stoof ze al roerend in het braadvet van de kip. 

* Leg na enkele minuten de stukken kip op de gestoofde groente en schenk er de marinade over, en ook de wijn. 

* Leg enkele takjes rozemarijn bovenop het stoofpotje en zet het deksel op de tajine. 

* Laat het stoofpotje 45 tot 60 minuten sudderen op een matig tot zacht vuur. Controleer tussendoor het resultaat. Het vlees van de kip moet bijna van de botjes vallen. Voeg naar smaak wat peper van de molen en een snuifje zout toe.

### afwerking: 
* Snij een gewassen citroen eerst middendoor en vervolgens in kleine partjes. Hak de (ontpitte) zwarte olijven grof. 

* Strooi de partjes citroen en de stukken olijf over de stoofschotel en laat ze nog kort meegaren. 

* Hak de peterselie grof en strooi ze kort voor het serveren over het gerecht. 

* Zet de warme tajine meteen op tafel en tast toe!