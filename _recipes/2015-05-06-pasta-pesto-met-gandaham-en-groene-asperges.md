---
layout: recipe
title: Pasta Pesto (met gandaham en groene asperges)
favorite: false
show: true
source: Au Bain-Marieke
source_url: http://aubainmarieke.be/recipe-items/pasta-pesto-met-gandaham-groene-asperges/
duration: "20 min"
tags: []
---

### Ingrediënten

### Bereiding 

1. Maak eerst de pesto: Doe het teentje knoflook, basilicumblaadjes, pijnboompitten en parmezaanse kaas in een keukenmachine en maal fijn. Voeg het citroensap en een scheutje olijfolie toe en breng op smaak met een beetje zwarte peper en eventueel wat zout.

2. Was de asperges en verwijder de onderste 3 cm en snij de rest in stukken van ca 5 cm. Kleine groene asperges snij je gewoon in 2.

3. Zet een pan met een ruime hoeveelheid water op het vuur en kook de asperges ca 5 min beetgaar. (kleine asperges veel korter) Schep ze met een schuimspaan uit de pan en kook de pasta gaar in het water van de asperges (vul eventueel nog aan met wat water).

4. Verhit een eetlepel olijfolie in pan en roerbak de asperges een minuut of twee.

5. Giet de pasta af en schep door de asperges. Voeg daarna de pesto toe en meng door de pasta en asperges.

6. Rooster de pijnboompitten.

7. Schep de pasta op een bord en doe een schep ricotta er boven op. Scheur de ham in reepjes en verdeel deze samen met de geroosterde pijnboompitten over de pasta en ricotta.