---
layout: recipe
title: Hartige taart met cherrytomaatjes
favorite: true
show: true
source: EVA
source_url: http://www.donderdagveggiedag.be/recepten/hartige-taart-met-cherrytomaatjes/
duration: "30 min"
tags: [quiche, veggi]
---


### Ingrediënten

* 5 vellen filodeeg
* 1 kleine bloemkool
* 1 rode ui
* 1 teentje look
* 350g cherrytomaatjes
* snuif peper en zout

### Bereiding

1. Verwarm de oven voor op 200 graden.
2. Snijd de bloemkool in kleine roosjes en kook in ongeveer 12 minuten gaar in een pannetje met water en wat zout (je moet er heel makkelijk met een vork in kunnen prikken).
3. Halveer ondertussen de ui en snijd in ringen.
4. Verwarm een scheut olie in een koekenpan, pers de knoflook uit en bak samen met de ui en de blaadjes van de takjes tijm in ongeveer 10 minuten op laag vuur. Voeg op het laatst wat peper en zout toe.
5. Vet een quichevorm in met wat olie (een springvorm kan evt. ook) en bedek de bodem en de randen met het filodeeg.
6. Giet de bloemkool af, laat goed uitlekken en pureer samen met een flinke snuf peper en zout met een stamper tot een fijne puree (dit kan evt. ook in een keukenmachine). Verdeel de puree over de bodem.
7. Verdeel het uimengsel hierover.
8. Halveer vervolgens de tomaatjes en verspreid over de bodem. Maak af met wat peper en zout.
9. Bak in 20 minuten in de oven en serveer warm.
