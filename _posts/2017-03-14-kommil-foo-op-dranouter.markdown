---
layout: post
title: Kommil Foo op Dranouter
image:
    feature: "content/kl-kommilfoo_in_concert_jaap_reedijk-_1.jpg"
date: '2017-03-14 09:00:07'
categories:
- festival
---

Het is geen geheim dat ik enorme fan ben van Kommil Foo, na de try-out van hun laatste show moest ik de show ook nog een tweede keer gaan zien.

Het [nieuws](http://www.festivaldranouter.be/nl/nieuws/2017-03-13/derde-reeks-namen) vandaag dat ze ook naar [Dranouter](http://www.festivaldranouter.be/nl/line-up/zondag/kommil-foo) komen maakte me dan ook uitermate blij.

----
Ik klikte ook willekeurig op een [andere artiest (Federspiel)](http://www.festivaldranouter.be/nl/line-up/zondag/federspiel) en smeet die in [Apple Music](https://itun.es/be/lY3e1), bij deze staan ze ook op mijn verlanglijst om te gaan zien 👌🏼
