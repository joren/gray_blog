---
layout: post
title: Welcome To My Garden bikepacking edition
date: 2020-08-03 17:23 +0200
tags: [cycling, bikepacking, travel, belgium]
---

Toen ten midden van de COVID-19 pandemie duidelijk werd dat we misschien niet meteen buiten onze landsgrenzen op vakantie zouden mogen gaan, of dat dit toch ten afgeraden zou worden, lanceerden enkele enthousiaste fietsers en stappers [Welcome To My Garden](https://welcometomygarden.org).

Het concept is simpel, je stelt je tuin beschikbaar voor mensen die er willen komen kamperen voor 1 nachtje. Dit enkel voor zij die te voet of met de fiets passeren.

![Mountainbike trail with village in the distance](/img/content/bikepacking_ardennes/IMG_6930.jpeg)

Na enkele bezoekers in onze tuin was het dit weekend onze beurt om er op uit te trekken.
Dus wij met de fiets op de trein naar Luik. Helaas heeft de NMBS het fietsers niet echt gemakkelijker gemaakt om de trein te nemen, maar we vonden toch nog 1 trein waar we op mochten stappen.

Van daaruit reden we in 3 etappes een deel van de [The Ardennes Arbalete](https://bikepacking.com/routes/ardennes-arbalete/), een bike packing route dwars door de Ardennen.

![Highest point in Belgium](/img/content/bikepacking_ardennes/IMG_6940.jpeg)

We sliepen bij Catherine en Fabienne waar we telkens hartelijk werden ontvangen en een stukje van hun tuin kregen en een emmer water om ons wat te wassen. Meer niet, maar dat is ook de afspraak. Geen fancy camping, genoeg afstand en gewoon een stukje van de tuin.
![Bikes with tent](/img/content/bikepacking_ardennes/IMG_6960.jpeg)
![Sunset tent](/img/content/bikepacking_ardennes/IMG_6962.jpeg)

En of we genoten hebben!  
Heerlijk! 

![Happy cow](/img/content/bikepacking_ardennes/IMG_6965.jpeg)
