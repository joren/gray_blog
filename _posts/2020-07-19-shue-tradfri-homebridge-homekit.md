---
layout: post
title: Combining IKEA Tradfri, Philips Hue Bridge with Homebridge to use them with Siri and Apple HomeKit
date: 2020-07-19 16:53 +0200
tags: [ikea, hue, tradfri, raspberry pi, automation, siri, homekit]
---

## Philips Hue
Since we moved a couple of months ago it itched to finally get some Philips Hue lights. At first, I just wanted a couple of them and use them as secondary ambiance lights in our living room.

I was immediately sold and wonder now why waited for so long. The thing that was always holding me back is that I did not want to replace my regular light switched or had to use my voice or phone to turn the lights on and off. But now I was walking around to separately turn on and off each extra light and this is now done with a single button. Also setting the mood when we’re watching TV or wanna read is great.

But these lights are expensive. We initially got a starter kit with 3 lights and the HUB. Soon after we wanted to get some more for my home office and our bedroom. (In the bedroom we do wanna use our voice to turn off the lights when we’re too lazy to reach for the switch).

## IKEA TRÅDFRI
So I started looking at how to connect IKEA Tradfri bulbs to my Philips Hue Bridge. Which seemed pretty easy:

1. You turn the light on and off 6 times to reset the light
2. Turn off the light
3. Open the Hue app and search for a new light
4. Turn on the light and it should appear

Step 2 really helped me here. You might also have to click the `link`-button on your bridge, but I never had to do that.

With these simple steps, you now have your IKEA lights in your Hue app. But they are still missing in Apple HomeKit. Since I build some automation steps in there, and I want to use Siri as well, this was pretty essential.

## Homebridge
This is where https://homebridge.io comes into the play. This is a pretty amazing tool that servers as a bridge between an endless list of things and Apple HomeKit. You can install it on your computer, or on a Raspberry Pi as I did.
During the COVID-19 lockdown we were in, we both worked form home. To make clear to my partner that I was in a meeting, I used it to light up 3 red LED’s that were connected to my Raspberry Pi. And I also have a [little script](https://github.com/mrfoto/slacker) running build by a colleague to manage my Slack status more easily.

Installing is pretty easy, just follow the guides: https://github.com/homebridge/homebridge

Once installed I used the [Homebridge-Hue](https://github.com/ebaauw/homebridge-hue#readme) plugin to connect it with my Philipe Hue Bridge. You can use the ConfigUI plugin to manage the settings or add these lines to you Homebridge config:

```
{
    "name": "Hue",
    "anyOn": true,
    "hosts": [
        "192.168.168.5"
    ],
    "lights": true,
    "linkButton": false,
    "nativeHomeKitLights": true,
    "nativeHomeKitSensors": true,
    "nupnp": true,
    "resource": true,
    "platform": "Hue"
}
```

It says to provide the Hostname and port of you Hue Bridge, but just the IP worked fine for me.
After that restart your homebridge and click the `link`-button on your Hue Bridge to connect.

My settings are ignoring the native Philips lights to show up in Homebridge so I do not have those twice in Apple HomeKit.
But once these steps are done, and you don’t see any errors in the Homebridge log, you should see the IKEA lights in the Accessories of you Homebridge and not much after also in your Apple HomeKit.

## What’s next
I have not yet tried to connect other Tradfri accessories such as switches or sensors as that might require some more fiddling. But I’m pretty happy with my current setup and will probably add more IKEA lights to it.

### Links of the stuff that I used/bought

* [https://github.com/homebridge/homebridge](https://github.com/homebridge/homebridge)
* [https://github.com/mrfoto/slacker](https://github.com/mrfoto/slacker)
* [https://github.com/ebaauw/homebridge-hue#readme](https://github.com/ebaauw/homebridge-hue#readme)
* [https://github.com/oznu/homebridge-config-ui-x#readme](https://github.com/oznu/homebridge-config-ui-x#readme)
* [https://www.kiwi-electronics.nl/raspberry-pi-4-model-b-plus-basic-pack-black-grey-2gb](https://www.kiwi-electronics.nl/raspberry-pi-4-model-b-plus-basic-pack-black-grey-2gb)
* [https://www.coolblue.be/en/product/838276/philips-hue-white-ambiance-starter-pack-e27.html](https://www.coolblue.be/en/product/838276/philips-hue-white-ambiance-starter-pack-e27.html)
