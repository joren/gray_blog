---
layout: post
title: Focussen op wat wel mag
date: 2021-02-21 17:46 +0100
tags: [corona, 52weeks]
---

Het voelt soms alsof we met z’n allen niet heel veel geleerd hebben het voorbij jaar als het gaat over wat wel en wat niet werkt tegen de verspreiding van het coronavirus. Daardoor lijkt het alsof we vastzitten in een slingerbeweging van heel streng naar te los.

En dit terwijl er wel degelijk een gulden middenweg bestaat. Alleen blijft de media nogal vaak hangen in wat niet mag. De gulden middenweg is natuurlijk saaier. 

Dat het voor iedereen anders is, is normaal. Wel of geen werk, kinderen, partner, een aangename thuis,... 
De volgende lijst is dus enkel wat goed werkt voor ons. Maar ik denk wel dat het voor veel mensen kan werken als je loslaat wat er allemaal niet mag.

Wij zijn dus blij dat we dit nog allemaal mogen doen:

* Af en toe eens baantjes trekken in het zwembad
* Gaan joggen
* Gaan fietsen
* Wandelen met vrienden
* Wandelen met familie
* Alleen gaan wandelen
* Gaan winkelen wanneer ik wil
* Na al dat gewandel buiten een ijsje eten
* Enkele dagen naar de Ardennen gaan met ons twee
* Met de fiets de trein nemen om zo lekker enkel met de wind mee naar huis te bollen.
* Eens om de twee weken met collega’s in een groot zaaltje de koppen samen steken voor enkele uurtjes
* Mee mogen naar de echo 🤰🏻
* Frietjes gaan halen in de frituur.

En dat gewoon steeds lekker met max 4 personen en indien nodig eens met een mondmasker

En tuurlijk mis ik heel veel dingen, maar ben vooral blij met alles wat nog wel mag.
