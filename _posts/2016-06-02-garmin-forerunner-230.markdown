---
layout: post
title: De Garmin Forerunner 230
image:
    feature: "content/iu.png"
date: '2016-06-02 14:07:31'
categories:
- lopen
- fietsen
- garmin
- strava
- technologie
---

Een jaar geleden begon ik met de hulp van [my.asics](http://my.asics.com) zeer op het gemak weer wat te lopen. Eigenlijk met als doel de 20km van Brussel, maar dat gaat helaas dit jaar niet lukken.  
Na de zeer langzame opstartfase begonnen de kilometers al te tellen en werd het belangrijker om ook te weten hoe snel je loopt. Hier liep het vroeger altijd fout, ik liep altijd veel te snel, was dus ook snel kapot en kreeg last van mijn knieën, deze keer wou ik het dus beter aanpakken.

Omdat lopen met je smartphone niet altijd even aangenaam is en als je met twee loopt wil je ook zonder oortjes lopen en hoor je dus niet wat bijv. Strava te melden heeft hoe snel je loopt en hoe ver je al zit.  
In januari vond ik dan ook dat ik al lang genoeg bezig was om een horloge te kopen dat daarbij kon helpen. 

Ik had 3 opties:

1. Een Apple Watch, maar wegens gebrek aan GPS zou ik dan nog wel steeds moeten gaan lopen met mijn iPhone.
2. Een simpel horloge dat ik kon koppelen met mijn bestaande hartslagmeter.
3. Een full-blown sporthorloge met GPS.

Voor de eerste optie is het volgens mij nog te vroeg, ik wist ook helemaal niet of ik nog wel een horloge heel de dag zou verdragen, en als dat niet het geval zou zijn bleef het wel een dure aankoop.  
De tweede optie is blijkbaar niet mogelijk, tenzij je naar de duurdere sporthorloges gaat.  
Optie 3 leek dan de beste keuze en de vriendelijke mensen van de AS Adventure wisten me te overtuigen van de Garmin Forerunner 230.

En dat blijkt nu een van de betere aankopen van het laatste jaar.  De batterij gaat 10u mee als de GPS op staat, jawel, 10u, je kan dus een hele dag gaan fietsen en je route tracken (wat ik ondertussen ook al [enkele](https://www.strava.com/activities/577970898) [keren](https://www.strava.com/activities/577080851) [deed](https://www.strava.com/activities/576066237)). Heerlijk is dat.

Als ik twee keer per week ga lopen, elke dag mijn woon-werk verkeer track en mijn notifications laat toekomen op mijn horloge kan ik een dikke week doen met 1 oplaadbeurt, wat ik zelf helemaal niet slecht vind.

Het is gemakkelijk en snel om een activiteit te starten, hij heeft meteen GPS signaal waardoor je ook direct kan beginnen lopen of fietsen.

Je hebt een minimale app-store met andere watch-faces en enkele leuke applicaties zoals het tracken van een wandeling of hike wat er standaard niet in zit.

De Garmin applicatie is niet zo proper en duidelijk als Strava maar je kan deze gemakkelijk koppelen aan elkaar waardoor je na je activiteit automatisch alles naar Strava stuurt.

Ik ben alvast fan en zou dit horloge aan iedereen aanraden die wil gaan sporten zonder smartphone en toch ook wat voordelen wil hebben van heel het smart-watch gebeuren zoals notifications.
