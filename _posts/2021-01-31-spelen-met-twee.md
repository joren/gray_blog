---
layout: post
title: Spelen met twee
date: 2021-01-31 19:59 +0100
tags: [52weeks, covid19]
---

Er zijn niet zoveel gezelschapsspellen die ook goed in elkaar zitten om met twee te spelen.
Toch vonden we het laatste half jaar enkele spelletjes die hier meermaals uit de kast gehaald worden. Speel je dus graag met twee, dan kan ik deze ondertussen wel aanraden:

## 1. Mandala

Een kaartspel met een niet al te groot aandeel in geluk, maar wel meer tactiek. Je moet je focussen op verschillende onderdelen van het spel en ervoor zorgen dat je niet te snel maar ook niet te laat reageert.

Koop her [hier](http://www.spelgezel.be/spel-review/mandala.php)

## 2. Keer op keer

Voor wie graag eens een variant zoekt op Yahtzee. Simpele regels waar het vooral belangrijk is om de juiste beslissingen te nemen.

Koop het [hier](http://www.spelgezel.be/spel-review/keer-op-keer.php)

## 3. Clever

Opnieuw een dobbelspel dat je ook met meer dan twee kan spelen en wat hier momenteel onze favoriet is. In verschillende kleuren moet je de juiste vakken proberen te vullen. Het leuke is dat wanneer je het slim aanpakt, je tegen het einde van het spel leuke combo’s kan maken en zo nog in de laatste ronde het spel kan winnen.

Deze moet je zeker [kopen](http://www.spelgezel.be/spel-review/clever.php)
