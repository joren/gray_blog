---
layout: post
title: Herbruikbare luiers
date: 2021-03-27 10:39 +0200
tags: [52weeks, mini]
---

De keuze is nog nooit zo groot geweest. Gemaakt van katoen, hennep of bamboe. Vouwbare, in 1 of twee stukken. Voor overdag, ‘s nachts of om te gaan zwemmen. Slank of super absorberend. In verschillende maten of laten meegroeien van maand 1 tot het einde. En in alle mogelijke kleuren en motieven.

Daarnaast uiteraard een pak minder afval en met wat zorg kan je ze voor meerdere kindjes gebruiken. Zo zou het ook goedkoper zijn. Daarbovenop zou onze mini er ook sneller zindelijk van worden. 

Wij zijn verkocht. Voor de eerste maand gaan we een pakket huren en daarna kopen we een setje dat mee zou moeten groeien tot het niet meer nodig is.

Met dank aan de [Luierhoek](https://www.luierhoek.be) voor de fijne infosessie, maakt de keuze toch wat gemakkelijker als je eens kan zien hoe al die systemen werken.
