---
layout: post
title: Pincode veiliger dan Touch ID
image:
    feature: "content/IMG_1463-1.jpg"
date: '2015-06-23 15:29:21'
categories:
- apple
---

Nu had ik gedacht dat wanneer je een paar keer je pincode fout ingeeft en je iPhone geblokkeerd is voor x aantal minuten, je je toestel wel nog zou kunnen ontgrendelen met Touch ID.
Met je vingerafdruk ben je toch zeker dat jij het bent en niet iemand die zomaar toegang wil tot je telefoon.

Nog sterker, wanneer de x aantal minuten gepasseerd zijn kan je je telefoon niet meer ontgrendelen met je vingerafdruk maar moet je dus eerst een correcte pincode ingeven. 

Dit is ook het geval wanneer je je telefoon herstart wat ik ook al vreemd vind vermits mijn code wel geraden kan worden maar mijn vingerafdruk niet.

Kijk, zo leren we toch ook nog eens iets bij. Apple vindt je pincode dus nog steeds veiliger dan je eigen vingerafdruk.
