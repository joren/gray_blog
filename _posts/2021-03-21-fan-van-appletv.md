---
layout: post
title: Fan van AppleTV+
date: 2021-03-21 19:11 +0100
tags: [52weeks, apple, tv, streaming]
---
En nee, niet alleen omdat het voorlopig nog gratis is en steeds verlengd wordt. Maar gewoon omdat de content echt goed is. Omdat er ook geen duizenden shows op staan is het gemakkelijker kiezen.
Niet zoals bij Netflix waar je ofwel reeds weet wat je wil kijken, of je verplicht wordt om hun aanbevelingen te volgen. Echt bladeren tussen series of docu’s geef ik al snel op.

Zoals velen begon ik bij AppleTV+ met The Morning Show, het duurde enkele afleveringen voordat ik er helemaal in zat maar wat een topreeks.

Vervolgens keken we naar Defending Jacob en verslond ik Ted Lasso.
Deze laatste keken we recent nog eens samen, ik voor de tweede keer. Alleen al voor deze show kan ik aanraden om een abonnement te nemen. Er zit geen woord teveel in en elk personage is perfect gecast. Ik kan niet beloven dat ik niet vanaf aflevering 1 begin te kijken als het tweede seizoen uitkomt. Deze wereld heeft meer Ted Lasso’s nodig!

Ondertussen begonnen aan Tehran en al twee afleveringen met klamme handen en voeten zitten kijken. Zo heerlijk spannend.

Het is duidelijk dat ze meer de HBO-toer opgegaan zijn, kwaliteit boven kwantiteit. Waardoor ik elke nieuwe reeks met plezier een kans ga geven. En na de trial zeker zonder nadenken ga verlengen.
