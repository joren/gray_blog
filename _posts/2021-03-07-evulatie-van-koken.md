---
layout: post
title: Evulatie van koken
date: 2021-03-07 20:24 +0100
tags: [52weeks, koken]
---

Bij ons thuis en in de familie werd er altijd graag gekookt en genoten van lekker eten. Het was geen uitzondering dat mijn papa een halve zondag in de keuken stond.
Toch duurde het even voor dit er bij mij uit kwam. Mede omdat koken voor 1 niet echt als de moeite aanvoelde, maar ook omdat ik niet graag met een boek of iPad naast mij iets nieuw wou uitproberen.

Net zoals bij velen kwam hier verandering in met de hulp van Jeroen Meus en Dagelijkse kost. Plots kreeg je elke dag te zien hoe je iets lekkers kon maken. Dit werkte veel beter dan een recept te moeten volgen op papier.

Fast forward enkele jaren. Met meer ervaring en techniek in de vingers werd het steeds minder noodzakelijk om stap voor stap te volgen wat ik op het scherm zag. De verzamelde recepten werden dus steeds minder boven gehaald.

Op dat moment kwam 40 dagen zonder vlees en de kennismaking met EVA VZW. Door 40 dagen verplicht zonder vlees te koken had ik weer wat nood aan inspiratie en hierbij kwamen de recepten van EVA goed van pas. Na afloop zorgde dit ervoor dat ik in de winkel niet steeds terugviel op iets met vlees, maar nu een bredere keuze had en steeds vaker koos om zonder vlees te koken.

Een jaar of twee later beslisten we dan ook om thuis altijd zonder vlees te koken. Deze stap was toen eigenlijk al niet meer zo moeilijk en het maakte de inkopen een stuk gemakkelijker.

Ondertussen kook ik nog zelden met een recept. Met uitzondering van de leuke tips die ik elke donderdag in mijn mailbox krijg van EVA (schrijf je ook in onderaan op deze pagina: https://www.evavzw.be/nieuws).
Zo plaats ik onze favorieten in een online lijstje en heb een perfecte fallback als ik zonder inspiratie zit. 

Kan nu zelf ook genieten van uitgebreid te koken voor ons twee of voor anderen. Maar evengoed van snel snel iets op tafel te toveren dat ook min of meer voor gezond door kan gaan.
En gelukkig wordt dat hier thuis voldoende geapprecieerd. 

Smakelijk!
