---
layout: post
title: Sneeuwbanden op mijn fiets
date: '2017-11-30 22:45:20'
image:
    feature: content/sneeuw.jpg
categories:
- fietsen
---

Ik had weer eens een goed idee. Laat ik nog snel even om half 11 mijn andere banden op mijn fiets zetten, het was toch best glad vanavond met mijn dunne bandjes...
Eerst de voorband, ging lekker vlot, nieuwe band erop, oppompen en achteraan beginnen. Tot plots...

💥💥

Springt de binnenband, met tutende oren vaststellen dat die toch wat versleten was en waarschijnlijk iets te hard opgepompt. 

Voorband er dus terug af. Gelukkig had ik nog een extra band van op vakantie in Slovenië. Die erop, iets minder hard oppompen en na poging twee bleef de voorband alvast op zijn plaats. 

De achterband dan maar. Blijkbaar had ik nog een ook reserve binnenband voor het mountainbiken, dus voor de zekerheid die er maar opleggen. 
Gaat allemaal vlot, tot ik aan mijn wiel draai en er niet veel gebeurt. Uiteraard is die mountainbike buitenband breder dan mijn andere en past die niet meer tussen mijn spatbord achteraan...

Dan maar mijn gereedschapskoffer zoeken en beginnen sleutelen aan dat spatbord. Gelukkig kon ik dat nog wat verschuiven en kan ik ook dat probleem snel fixen. 

Wat een snel klusje moest zijn duurde zo misschien net iets langer. 
Dat er morgen dus maar best nog steeds sneeuw ligt zodat ik naar het werk kan knallen met nieuwe banden 😁
