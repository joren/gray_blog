---
layout: post
title: Vaarwel Dropbox en Pocket
date: '2017-01-01 13:56:35'
categories:
- technologie
---

Twee weken geleden werd mijn nieuwe laptop geleverd (zo een met een [Emoji-bar](http://www.apple.com/macbook-pro/)).
Een van de fijnere dingen aan een nieuwe laptop is weer proper kunnen beginnen, niet meteen alles terugzetten zoals voorheen maar enkel wat je echt terug nodig hebt.

Voor mijn werk is het voldoende om mijn [dotfiles](https://github.com/joren/dotfiles/tree/feature/test) terug te zetten en enkele [brew](http://brew.sh) en [mas](https://github.com/mas-cli/mas) commando's later staat alles weer netjes klaar voor mijn dagelijks werk te kunnen doen.

Maar verder wou ik ook af van enkele externe tools gebruiken, dingen die ik dan ook weer op mijn iPhone zou moeten installeren en die eventueel geld kosten.

* Dropbox, vanaf nu alles in mijn iCloud, voor mijn werk heb ik genoeg met de website.

* Pocket, eigenlijk past dit ook gewoon in mijn Leeslijst in Safari, zo staat het ook meteen op mijn iPhone en iPad en kan daar de pocket app ook weer weg.

Ik installeerde ook geen Twitter app meer en probeer niet meer in te loggen met mijn Google account om zo wat minder tijd te verspillen op Youtube.

Misschien past dat ook wel goed bij de start van het nieuwe jaar, een voornemen om wat minder op een scherm te kijken. Meteen ook al die apps van mijn iPhone verwijderd. Maar misschien daar meer over in een aparte blogpost.
