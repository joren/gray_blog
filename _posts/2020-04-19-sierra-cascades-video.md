---
layout: post
title: Sierra Cascades
date: 2020-04-19 18:33 +0200
tags: [bicycle, travel, koga, usa]
---

In 2018 reden we met ons twee, op de fiets, van Canada naar Mexico. Dwars door het westen van de Verenigde Staten.
We volgende de Sierra Cascades route door verschillende nationale parken.

Dit zal nog wel voor even onze meest memorabele reis blijven.

Elke dag filmde ik een stukje van onze tocht en maakte nu eindelijk een kleine compilatie

<div class='embed-container'>
<iframe src='https://www.youtube-nocookie.com/embed/6P5k_-Xkg-A' frameborder='0' allowfullscreen></iframe>
</div>

Wil je graag alles alle filmpjes bekijken, dan kan dat uiteraard ook [https://www.youtube.com/watch?v=8gK10W1jxDk](https://www.youtube.com/watch?v=8gK10W1jxDk)
