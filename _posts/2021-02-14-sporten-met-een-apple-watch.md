---
layout: post
title: Sporten met een Apple Watch
date: 2021-02-14 20:25 +0100
tags: [52weeks, apple, watch, review]
---

Puur om te sporten was ik lang tevreden van mijn Garmin Forerunner. Heb deze kunnen gebruiken om rustig op te bouwen naar een eerste marathon. Qua extra integratie met je telefoon is deze echter behoorlijk beperkt. Je kan wel notificaties bekijken, maar niets terugsturen. Ook kan je niet extra filteren welke notificaties je enkel op je telefoon wil krijgen en niet op je pols.
Wil je ook actief blijven en gemotiveerd worden kan dit met het Garmin horloge enkel door te stappen of lopen. Fiets je bijvoorbeeld een halve dag dan moet je nog steeds je 10.000 stappen doen om je dag actief af te sluiten.
Daarom werd de opvolger enkele jaren geleden een Apple Watch. Meer controle over de notificaties, interactie in twee richtingen en alle activiteit telt mee om je cirkels te sluiten elke dag.
Maar het was wel een kunst om de echte sport- en trainingsfunctionaliteiten te evenaren van mijn Garmin Forerunner. 
Na verschillende apps te testen ben ik wel content met wat ik nu gebruik.

## Lopen
Simpele loopjes registreer ik gewoon met de activiteitenapp van Apple. Ik zie mijn hartslag, duur, afstand en gemiddelde snelheid. Heel af en toe stel ik in dat ik slechts 1 metric zie als ik bijvoorbeeld puur op snelheid of hartslag wil lopen.

### Interval & workout
Meteen na de aankoop wou ik mijn Apple Watch gebruiken om te trainen voor een tweede marathon. En hier horen intervals en lopen op hartslag zeker bij. 
Al snel kwam ik bij [Simple Interval Timer](https://apps.apple.com/us/app/simple-interval-timer/id458334253) en [Zones](https://flaskapp.com/zones/), beide helaas net iets te gelimiteerd. Of er werd geen echte activiteit geregistreerd, of het was enkel nuttig als je ook af en toe naar je horloge kijkt, niet handig dus.
Via een tip kwam ik bij [iSmoothRun](http://www.ismoothrun.com) terecht. Beetje gekke naam, maar doet perfect wat ie moet doen. Zo kan je heel specifiek workouts en intervals instellen die je vervolgens via audio cues kan volgen. Wil je bijvoorbeeld lopen onder een bepaalde hartslag of een warmup gevolgd door verschillende intervals en een cooldown, dan kan je dit allemaal instellen.
De interface is wat zoeken, maar alles zit er wel in. In de laatste update zit nu ook de mogelijkheid om trainingschema’s te synchroniseren met externe diensten, maar dat heb ik nog niet getest.
![Ismoothrun interval example](/img/content/2021/02/ismoothrun.png){: class='half' }

### Route volgen

Dé app die ook tijdens het lopen, of andere activiteit, het toelaat om een GPX te volgen met kaart is voor mij [WorkOutDoors](http://www.workoutdoors.net). Importeer je GPX bestand op je telefoon, stuur de route naar je horloge en klaar. Zelfs als je geen gsm mee wil nemen kan je het deel van de kaart waarlangs je route loopt downloaden op je horloge. Op deze manier heb ik veel leuke nieuwe routes kunnen lopen. Zeker op vakantie een must wanneer je gewoon door wil kunnen lopen zonder bij elke kruising je gsm boven te moet halen.

## Fietsen
Fietsen doe ik steeds gewoon met de activiteitenapp. Wanneer ik meer specifieke intervals doe dan gebruik ik liever mijn Garmin GPS, toch iets handiger om snel te zien waar ik mee bezig ben.
Alleen jammer dat er geen auto-pause functionaliteit in zit zoals bij het lopen. Helaas al meermaals vergeten terug te activeren na een pauze.

## Zwemmen
Ook zwemmen doe ik eigenlijk enkel met de standaard activiteitenapp. Gemakkelijk om snel de afstand van het zwembad in te geven en je bent vertrokken. Trainingen doe ik meestal in clubverband waardoor de instructies niet van mijn horloge moeten komen.
Helaas hier ook hetzelfde probleem met auto-pauzeren.

## Exporteren en statistieken
### HealthKit app

De eerste app die ik installeerde was [Healthfit](https://apps.apple.com/us/app/healthfit/id1202650514). Deze laat toe om je activiteiten te synchroniseren met Strava of gelijkaardige andere diensten. Je kan dit volledige automatisch laten lopen, puur manueel of per type sport een aparte instelling.
De voorbije jaren kwamen hier veel extra functionaliteiten bij qua ingebouwde statistieken. 
Een mooi voorbeeld van hoe de Fitness app van Apple er zou mogen uitzien. Zeker de aankoop waard.
![HealthFit screenshot past 12 months cycling](/img/content/2021/02/healthfit1.png){: class='half' }
![HealthFit screenshot Year To Date Running](/img/content/2021/02/healthfit2.png){: class='half' }
![HealthFit screenshot Year compare walking](/img/content/2021/02/healthfit3.png){: class='half' }

## Conclusie
Met de standaard workouts app geraak je al een heel eind, je kan kiezen om meerdere datapunten te zien of slechts 1 als je je daarop wilt focussen. Zo goed als alle sporten zijn beschikbaar en je workout is exporteerbaar inclusief al de GPS en hartslag data.

Wil je echter wat meer specifiek gaan trainen op hartslag en specifieke intervals, dan raad ik je aan om de volgende apps te kopen. Zo maak je van je Apple Watch een volwaardig sporthorloge.
Ik mis mijn Garmin Forerunner alvast niet.
