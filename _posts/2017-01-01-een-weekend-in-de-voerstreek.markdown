---
layout: post
title: Een weekend in de Voerstreek
image:
    feature: "content/IMG_4600.jpg"
date: '2017-01-01 13:34:19'
categories:
- verlof
---

Wegens een gebrek aan sneeuw in de Vogezen moesten we een alternatief zoeken voor ons geplande sneeuwstap-weekend.
Last-minute dus nog een chalet geboekt op een kamping midden in de Voerstreek. Ik was hier nog nooit geweest besefte ik toen ik de GPS klaarzette. Ik moet zelfs bekennen dat ik het enkel kende door de communotaire strubbelingen in de regio waardoor ik zelfs dacht dat deze streek zich ergens rond het Brussels bevond. *\*bloos\**

Eenmaal af de autostrade heb je het gevoel dat je aan de rand van de Ardennen staat, maar iedereen begroet je wel met een Limburgs accent.

Meteen de stapschoenen aan en 2 dagen lang alle wandelknooppunten in de buurt eens gaan verkennen. En of dat het de moeite was, echt een prachtige streek. Veel op en neer, door de bossen, velden en boomgaarden. Langs kastelen, beekjes en kleine gezellige dorpjes. 

En na het wandelen onszelf getrakteerd op een klein bezoekje aan [appelwijn.be](http://appelwijn.be), ook een aanrader als je daar in de buurt bent. Als je met je ogen dicht zou proeven zou ik niet kunnen zeggen dat de wijn die we geroefd hebben niet van druiven gemaakt werd.

Ons zien ze daar zeker nog eens terug, misschien eens met de koersfiets of de mountainbike.
