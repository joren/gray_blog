---
layout: post
title: Met de telefoon achter het stuur
date: 2021-03-14 16:57 +0100
tags: [52weeks, frustraties, verkeer]
---
Je haalt ze er zo uit. Kopke naar beneden, even moeten corrigeren na een zwalp, ogen allesbehalve op de weg.
En helaas steeds meer en meer. Moet je eens op letten als je aan de lichten staat te wachten of als passagier meereist.

Doet 🙏 Dit 🙏 Niet.

Dat sms’je kan wel wachten, dat telefoontje of mailtje ook. Kom ook gewoon niet in de verleiding, leg je gsm weg of zet deze in auto-modus.

En hoor je of weet je dat iemand in de auto zit, zeg dan dat je later wel terug zal bellen of dat je niet meteen een antwoord verwacht en dat ze beter even naar de weg kijken.
Als voetganger of fietser voelt dit des te dringender als je merkt dat een tegenligger je niet ziet maar wel het scherm voor zich. En dat loopt vroeg of laat eens slecht af.

Dus denk aub niet dat jij dit wel wel kan en leg ‘m weg. De rest dankt u.
