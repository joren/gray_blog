---
layout: post
title: Fertiliteitstraject tijdens Corona
date: 2021-01-23 16:25 +0100
tags: [52weeks, covid19]
---


Enkele jaren geleden besloten we dat het tijd was om ervoor te gaan. We hadden net [een zotte reis](https://joren.gent/2020/04/19/sierra-cascades-video.html) in de benen, werk, een thuis en liefde in overvloed.
Helaas liep alles niet zoals gehoopt en klopten we in 2019 aan bij de afdeling fertiliteit om ons te helpen. 
De rollercoaster aan emoties in de daarop volgende maanden had ik niet zien aankomen. De eerste stimulaties wierpen ook geen vruchten af en ook kon er niet meteen een oorzaak gevonden worden.
Begin 2020 begonnen we dan aan een volgende stap: IVF. De hoeveelheid spuiten werden verhoogd alsook de spanning en emotie.

En dan vlak voor onze eerste poging kwam Corona.

## Gesloten afdelingen
Alsof het nog niet lastig genoeg was kwam er plots de extra stress of alles überhaupt wel zou mogen doorgaan. De onduidelijkheid over wat wel en wat niet meer zou mogen was belachelijk. Ook de verschillen in procedures tussen de ziekenhuizen, zelfs in dezelfde stad, waren om uw haar van uit te trekken. Zo bleef het ene ziekenhuis wel nog opgestarte behandelingen afronden daar waar anderen enkel behandelingen lieten doorgaan voor vrouwen ouder dan 40.
Midden maart hadden we dan toch het geluk om, met enkele andere koppels, als laatste patiënten onze eerste poging af te werken.

## Partner niet toegelaten
“We” is hier wel veel gezegd. Ik mocht als partner al niet meer mee met de pick-up of de terugplaatsing. Ook al moest ik op de dag van de pick-up ook in het ziekenhuis zijn voor mijn deel van de taak.
Alsof zwanger worden enkel iets is voor de vrouw, mochten de mannen buiten op een bankje wachten.

Zo gingen er 2 pogingen voorbij zonder resultaat (je krijgt er maar 6 terugbetaald). We besloten dan om naar Jette te gaan.

Maar ook in Jette geen beterschap voor de partner. Ik werd eerst in het ziekenhuis verwacht voor mijn taak waarna mijn vrouw mocht gaan wachten in een eenpersoonskamer terwijl ik buiten mij kon bezighouden.

### Goed nieuws

Als man word je al snel in dit proces buiten spel gezet. Medisch snap ik dat, maar als je met twee bent wil je dat ook met twee dragen. Je doet deze dingen niet elke dag en je wil hier dus samen door, al was het maar om als nozem langs de kant van het bed te staan.

Uiteindelijk kwam er ook eens goed nieuws. Niet zonder extra spanning, want plots kwam er een tweede lockdown op ons af. 
In Jette maken ze er punt van om de partner erbij te betrokken. Dit klopte zeker bij onze eerste gesprekken, er werd voor de eerste keer ook aan mij gevraagd hoe het ging.
Maar voor onze eerste echo zou ik er niet bij mogen zijn. Dit was de eerste keer dat mijn vrouw haar connecties wat gebruikt heeft om er toch voor te zorgen dat ik mee mocht komen.
Triest dat het alleen zo kon, en ook alleen voor ons genoot ik wel dubbel van de eerste beelden van twee kloppende hartjes. Dit zou lang zijn blijven hangen als ik dat vanuit de wagen had moeten meemaken.

### Slecht nieuws

Een week later gingen we weer op controle, ook nu via een omweg omdat ik er anders niet bij mocht zijn. 
Deze keer was er nog maar 1 hartje te zien.

Dat is zeker geen nieuws dat ik wou horen tweedelijns op de parking of via een schermpje.

## Zo hoeft het niet te gaan
Veel ingrepen in een ziekenhuis kan je gerust alleen ondergaan. Maar in de meeste gevallen doorloop je een fertiliteitstraject met twee. Je draagt met twee het slechte nieuws maar ook het goede. 
Het is als man al niet gemakkelijk om je nuttig te voelen in heel dit proces. Ik moest geen spuitjes zetten maanden aan een stuk, mijn lichaam maakte mij niets wijs en ik moest niet onder volledige of lokale verdoving eitjes laten wegnemen en bevrucht weer laten terugplaatsen. 
Maar ik maak er wel deel van uit, en dat kan niet op dezelfde manier vanuit de wagen.

Laat me desnoods buiten wachten tot wij aan de beurt zijn of steek me op mijn kosten in een hermetisch afgesloten pak. Maar in een traject dat zonder dit alles al zo ongelofelijk beladen is, heb je als koppel er echt baat bij dat je dit zoveel mogelijk samen kan doorlopen.

Nog steeds zijn er in verschillende ziekenhuizen andere regels, zowel voor goed nieuws als voor slecht nieuws. En dit ook voor andere ingrepen waar je wel graag iemand naast je hebt zitten als er goed of slecht nieuws op je afkomt.

Dit kan beter!
En dit is ook waarom ik mijn best doe om de regels zo goed mogelijk op te volgen. Niet enkel voor ons, maar zodat niemand zich alleen door zulke momenten moet wurmen.

Ondertussen gaat alles goed met onze mini 🥰
