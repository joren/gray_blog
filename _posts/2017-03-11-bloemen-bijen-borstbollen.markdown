---
layout: post
title: Bloemen Bijen & Borstbollen
image:
    feature: "content/17.04.28_wouter_deprez_ruben_focketyn_-_bloemen_bijen_en_borstbollen_c_jonas_lampens.jpg"
date: '2017-03-11 16:50:49'
categories:
- theater
---

Tranen gelachen en de krop in de keel.

Wouter Deprez vertelt over zijn tuin, kinderen en de ziekte van zijn vrouw. En dat allemaal met wat achtergrondmuziek van Ruben Focketyn.

Hij laat je lachen met banale situaties en mopjes over de kanker van zijn vrouw en pakt je vast bij de keel over hoe broos en moeilijk het is om met zo'n ziekte om te gaan en door te gaan.

Het was een topavond in de Vooruit, ik kocht pronto het [boekje met bijhorende cd](http://www.working-class-heroes-shop.com/wouter-deprez/bloemen-bijen-borstbollen) om het later nog eens op te kunnen zetten en te lezen.

Zoek zeker of je [ergens kaarten](http://www.wouterdeprez.be/#agenda) kan krijgen, het zal niet teleurstellen!
