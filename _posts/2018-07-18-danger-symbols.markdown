---
layout: post
title: Danger Symbols have to last forever
date: '2018-07-18 10:47:20'
categories:
- design
tags: [youtube, design, podcast]
---

Ik ben al jaren fan van de podcast [99% invisible](https://99percentinvisible.org) waardoor ik ook meteen op play klikte toen ik deze video zag passeren.

Een interessante kijk op de niet voor de hand liggende taak om symbolen te ontwerpen die, liefst zo lang mogelijk, gevaarlijke situatie moet kunnen identificeren.

<div class='embed-container'>
<iframe src="https://www.youtube-nocookie.com/embed/lOEqzt36JEM?rel=0" frameborder='0' allowfullscreen></iframe>
</div>
