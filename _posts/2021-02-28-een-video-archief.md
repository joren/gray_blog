---
layout: post
title: Een video archief
date: 2021-02-28 20:09 +0100
tags: [week52, vhs, OSS]
---

Ergens in een doos op zolder liggen al even enkele oude videocassettes samen met een videorecorder en een kabeltje om deze aan de computer te hangen. Je weet wel, voor _ooit_ eens als je wat tijd hebt.
Laten we nu plots wel wat tijd hebben tussen al het wandelen door.

Maar zoals dat gaat kreeg ik het belangrijkste deel niet aan de praat, dat extra kabeltje wou niet werken. Er zou ergens nog wat firmware op een cd’tje moeten staan, maar dat heeft de laatste verhuis precies niet overleefd.

Jammer maar helaas. Tot ik deze blogpost tegen kwam van [Cimm](https://twitter.com/cimm): [https://www.suffix.be/blog/family-youtube/](https://www.suffix.be/blog/family-youtube/)
Zo kwam ik terecht bij [Memorepair](https://memorepair.for-ever.com/nl/homepage) die zowat elk formaat van foto, dia of videocassette digitaliseert.

Zo stuurde ik begin dit jaar enkele cassettes op als test en een week of twee geleden kreeg ik de downloadlinks in mijn mailbox. Met een heerlijke nostalgische avond als gevolg.

Naast de tip voor deze dienst zette Simon zijn project om de video’s te delen ook nog eens op [Github](https://github.com/cimm/vhs/). Het enige wat ik moest doen was elke video in stukken opdelen en de JSON opvullen met elk deel. Daarna alles uploaden naar Backblaze B2 en online zetten.

En zo kon ik dit weekend ook de rest van de familie wat leuke beelden tonen van een kleine 30 jaar geleden. Nog eens leuk herinneringen ophalen aan zij 
die er niet meer zijn en onze ouders vergelijken met onszelf.
