---
layout: post
title: Een wekelijkse blog
date: 2021-01-10 14:31 +0100
tags: [52weeks, meta]
---

52 weken, 52 blogposts.

De laatste jaren was ik vooral goed in het uitbreiden van de lijst “blogpost ideas” in Things. Dit jaar is de tijd gekomen om deze lijst ook eens af te beginnen werken.

Ik heb al lang een soort van haat-liefde relatie met schrijven. Of het nu gaat over mails, blogposts, meeting-verslagen of -voorbereidingen. Ik wil vaak het hele verhaal reeds klaar hebben in mijn hoofd voor ik begin te typen. Waardoor ik maar blijf uitstellen en het er nooit van komt.
Het is dan ook niet enkel een uitdaging om de backlog van blogpost ideeën weg te werken, maar ook een oefening om meer structuur en plezier te vinden in schrijven.

Hopelijk gaat het mij zo wat forceren om al sneller aan een idee te werken, eens een paar dingen neer te schrijven en dit dan ook verplicht na te lezen. Dat laatste is een tweede probleem dat ik graag wil aanpakken: minder schrijffouten. Mijn taalgevoel is niet echt top, combineer dat dan met een gebrek aan goesting om na te lezen en je krijgt een hoop mails en teksten vol stomme typo’s. Ook onleesbare zinnen omdat ik halverwege de structuur wou veranderen zijn me niet onbekend.

Laat dit dus de start zijn van een leerproces en hopelijk enkele leuke blogposts over werk, reizen en familie.

Week 1: ✅
