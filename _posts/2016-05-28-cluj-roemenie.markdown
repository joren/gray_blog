---
layout: post
title: Cluj, Roemenië
date: '2016-05-28 12:15:02'
categories:
- verlof
---

Ik ben dit weekend te gast op een trouwfeest in Roemenië.  

Omdat er enkel op vrijdag een vlucht was zat ik 2 dagen 'vast' in Cluj. Gelukkig schijnt de zon hier en is het Transilvania International Film Festival net begonnen. Een ideale combinatie om mij niet te vervelen. 

![Cluj](https://i.imgur.com/Q4MnRKX.jpg)

Als je ooit Noord Roemenië wil zien en op Cluj vliegt dan moet je zeker een extra dagje in dit kleine stadje blijven. Leuke hoekjes, koffiebars, burgers en andere restaurantjes. 

![omnomnom](https://i.imgur.com/ezVIqDT.jpg)

En als je eens op een avond niet weet welke film op te zetten kan ik bij deze [Returning Home](http://m.imdb.com/title/tt3831000/) aanraden. Een pittige Noorse film over twee broers die hun vader gaan zoeken die langer dan gepland weg blijft tijdens het jagen nadat hij net terug is uit Afganistan. Een film in het prachtige Noorwegen, wat een land. 
