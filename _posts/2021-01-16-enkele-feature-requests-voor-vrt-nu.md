---
layout: post
title: Enkele feature requests voor VRT.nu
date: 2021-01-16 17:56 +0100
tags: [52weeks, feedback]
---

We hebben in het verleden al betaald voor televisie via Telenet, Stievie of Streamz. Maar door het beperkt aantal programma’s en zenders dat we hier thuis momenteel volgen voelde dat toch steeds wat te duur en overbodig.
De komst van VRT.nu maakte de overstap naar TV zonder een van deze diensten een pak gemakkelijker.

Het laat ons nu al enkele jaren toe om alles live of uitgesteld te kijken en daar zijn we zeer blij en dankbaar voor.

Ik ben me er van bewust dat er bij de VRT slechts een beperkt aantal mensen aan deze dienst werken en dat er ongetwijfeld met teveel meningen en regels rekening gehouden moet worden. Maar misschien missen ze gewoon ook wat feedback van gebruikers. Laat ik bij deze mijn feature requests even opsommen, sommige gaan er misschien nooit komen, maar enkelen lijken me niet meteen het meeste werk.

## 1. Een AppleTV app.
De iPhone en iPad app werken wel ok, maar er zijn toch wat probleempjes met AirPlay. Vooral live kijken lukt niet altijd even vlot. Vaak blijft die hangen in 360p kwaliteit ook al hebben we thuis een best goede internetverbinding. Maar het grootste probleem met live kijken is dat die 1 op de 2 keer helemaal flipt bij het starten van de stream zoals je hieronder kan bewonderen. Meestal moeten we even onze epilepsie onderdrukken en een minuutje wachten tot alles vlot door begint te spelen.

<video width="320" height="240" controls>
  <source src="/content/vrtnu.mp4" type="video/mp4">
  <source src="/content/vrtnu.webm" type="video/webm">
Your browser does not support the video tag.
</video>

Kijk gerust eens naar de VTM app op AppleTV, meer moet dat niet zijn 🙌🏼

## 2. De url
Dit stoort mij al sinds het begin en snap de reden hierachter niet meteen. Ik wil graag zo snel mogelijk bij de juiste website terecht komen. Maar enkel `vrt`  typen is niet genoeg. `vrt.nu` redirect namelijk naar `https://www.vrt.be/vrtnu/`. Op zich niet zo een probleem moest hun andere populaire website `http://vrtnws.be` niet dezelfde soort redirect doen en me naar `https://www.vrt.be/vrtnws/nl/` sturen.
Ik kom dus vaak op de verkeerde plek terecht als ik te snel de suggestie van mijn browser volg en kan dit enkel oplossen door net niet de volledige url van de redirect te typen.

## 3. Favoriete programma’s
Hier kunnen misschien wel met het minste werk de grootste verbeteringen bekomen worden.
We zijn allemaal Netflix, Disney+, AppleTV+ en andere diensten gewoon waarbij je toch net iets meer hebt aan je aangemaakt profiel.

Zo kan je bij de VRT.nu wel programma’s als favoriet markeren, maar ook enkel wanneer de eerste aflevering reeds uitgezonden werd. Maken ze dus op voorhand reclame voor een nieuw programma, dan kan je dit niet reeds markeren als favoriet vermits er nog geen pagina bestaat van het programma zolang de eerste aflevering niet gepasseerd is.

Eenmaal gemarkeerd als favoriet verstoppen ze deze lijst ook net niet onderaan op de homepage. Dit mag gerust wat meer naar boven verhuizen.

En waarom dit gewoon bij een lijst houden? Je weet welke afleveringen ik reeds bekeken heb. Het zou nog beter zijn wanneer je toont als er een nieuwe aflevering is en deze meteen afspeelbaar maakt vanop de homepage.

## 4. Geen TV gids op de iPad/iPhone
Hier is misschien een logische verklaring voor, maar waarom is er geen TV gids te vinden op de iPad app maar wel via de website?

## Conclusie 
Ik ben fan van de programma’s op Canvas en Een en dankbaar dat ik ze op deze manier kan bekijken. Het zou alleen nog vlotter werken met deze aanpassingen ;) 
En als jullie voor iets beta testers nodig hebben dan meld ik me bij deze graag aan.

PS: Kijk naar [We moeten eens praten](https://www.vrt.be/vrtnu/a-z/we-moeten-eens-praten/), nu al beste programma van het jaar

