---
layout: post
title: "Waarom nog betalen"
description: "Waarom nog betalen voor muziek"
category: music
tags: [music]
---


[Sabam sleept Belgacom, Voo en Telenet voor de rechter](http://datanews.knack.be/ict/nieuws/sabam-sleept-belgacom-voo-en-telenet-voor-de-rechter/article-4000290335695.htm).
Ze willen een deel van de inkomsten van je internetverbinding innen als om in hun auteursrechten-kas te steken.

Ik die betaal voor Spotify en af en toe nog eens een cd koop ga dus nog eens extra moeten betalen. Als ik dan ook nog in de auto wil luisteren moet ik betalen om die muziek op een 'drager' te zetten namelijk een cd of een mp3-speler.
Straks ga ik dus ok nog extra moeten betalen om deze muziek in de eerste plaats bij mij te krijgen.

Is dit echt de juiste manier om aan geld te geraken? Hebben gemakkelijkere en goedkopere diensten als Spotify en Deezer niet al bewezen dat er genoeg mensen zijn die willen betalen voor hun content.

Ik zou het een pak leuker vinden moesten ze allemaal eens wat meer tijd en moeite steken in het zoeken naar betere oplossingen om de mensen effectief te laten betalen ipv her en der geld te zoeken op plaatsen die niet juist aanvoelt voor de eindgebruiker.
